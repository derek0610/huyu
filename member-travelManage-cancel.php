<?php
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
}
if(!isset($_GET['orderNum'])){
    header('Location: member-travelManage.php');
    exit;
}

$member_sid = $_SESSION['user']['sid'];
$order_number = $_GET['orderNum'];

$o_sql = "SELECT * FROM `orders` WHERE 1 AND `order_number`=$order_number AND `belong`=$member_sid";
$o_stmt = $pdo->query($o_sql);
$o_row = $o_stmt->fetch();

$total = $o_row['total'];
$totalNum = preg_replace('/\D/','',$total);

$o_date = $o_row['date'];
$now_date = date("Y-m-d");
$b41_date = date("Y-m-d",strtotime("-41 day",strtotime($o_date)));
$b31_date = date("Y-m-d",strtotime("-31 day",strtotime($o_date)));
$b21_date = date("Y-m-d",strtotime("-21 day",strtotime($o_date)));
$b2_date = date("Y-m-d",strtotime("-2 day",strtotime($o_date)));
$b1_date = date("Y-m-d",strtotime("-1 day",strtotime($o_date)));


?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member-cancel.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<!-- --------------------------------------header--------------------------------------------- -->
    <div class="container">
        <header id="app3" >
                <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- ---------------------------------travelManage-cancel------------------------------------- -->
        <main>
            <section class="travelManage">
                <div class="orderNum"><h2>取消預約</h2><span class="num">訂單編號：<?= $o_row['order_number'] ?></span></div>
                    <div class="orderInfo">
                        <div class="orderTitle">
                            <h3><?php 
                                switch($o_row['type']){
                                case "walk":echo "浴衣體驗-散步方案";break;
                                case "photo":echo "浴衣體驗-攝影方案";break;
                                case "explore":echo "浴衣小旅行-探險路線";break;
                                case "history":echo "浴衣小旅行-軼聞路線";break; 
                            }?></h3>
                        </div>
                        <div class="orderDetail">
                            <div class="basicInfo">
                                <div class="term date"><h5>日期</h5><p><?= $o_row['date'] ?></p></div>
                                <div class="term time"><h5>時間</h5><p><?= $o_row['time']=="am" ? "上午 10:00" : "下午 14:00" ?></p></div>
                                <div class="term people"><h5>人數</h5><p><?= $o_row['people'] ?>人</p></div>
                            </div>
                            <hr>
                            <div class="basicInfo">
                                <div class="term plan"><h5>方案</h5><p><?php 
                                switch($o_row['plan']){
                                    case  "inside":echo "棚內拍攝/每人NT$1,000";break;
                                    case  "outside":echo "外景拍攝/每人NT$1,600";break;
                                    case  "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                    case  "two":echo "兩小時體驗/每人NT$500";break;
                                    case  "four":echo "四小時體驗/每人NT$700";break;
                                    case  "six":echo "六小時體驗/每人NT$850";break;
                                    case  "twoFour":echo "二至四人/每人NT$2,000";break;
                                    case  "fiveNine":echo "五至九人/每人NT$1,800";break;
                                    case  "tenUp":echo "十人以上/每人NT$1,500";break;
                                }?></p></div>
                                <?php if($o_row['extra']>0):?>
                                <div class="term add"><h5>妝髮</h5><p><?= $o_row['extra'] ?>人/每人 NT$500</p></div>
                                <?php endif;?>
                            </div>
                            <hr>
                            <div class="basicInfo">
                                <div class="term total"><h5>付款金額</h5><p><?= $o_row['total'] ?></p></div>
                                <div class="term total"><h5>付款方式</h5><p><?= $o_row['pay_method'] ?></p></div>
                                <div class="term paymentStatus"><h5>付款狀態</h5><p class="yes"><?= $o_row['pay_status'] ?></p></div>
                                <div class="term travelStatus"><h5>行程狀態</h5><p class="notYet"><?= $o_row['order_status'] ?></p></div>
                            </div>
                        </div>
                        <div class="cancelTitle">
                            <h3>取消預約規定</h3>
                        </div>
                        <div class="cancelRule">
                            <p>預約後因故不能參加旅遊行程時，請儘早進行線上取消；因下單後即為旅客處理出團事宜，若取消旅遊行程，須依旅客通知取消當日距離出發日期的長短賠償相關取消費用，應賠償之取消費用將依「國內旅遊定型化契約書」之規定辦理。</p>
                            <hr>
                            <p class="st">國內旅遊定型化契約書規定:</p>
                            <div class="ruleBox">
                                <ul class="rule">
                                    <li>出發前41天以前取消，將收取5%之費用。</li>
                                    <li>出發前31至40天取消，將收取10%之費用。</li>
                                    <li>出發前21至30天取消，將收取20%之費用。</li>
                                </ul>
                                <ul class="rule">
                                    <li>出發前2至20天取消，將收取30%之費用。</li>
                                    <li>出發前1天取消，將收取50%之費用。</li>
                                    <li>出發當天取消、無故未到者，恕不退費。</li>
                                </ul>
                            </div>
                            <p class="st">舉例：若行程出發日期為12/31</p>
                            <div class="ruleBox">
                                <ul class="rule">
                                    <li>於12/31出發日當天取消行程，恕不退費。</li>
                                    <li>於12/30取消行程，收取50%之賠償費用。</li>
                                    <li>於12/11-12/29期間取消行程，收取30% 之賠償費用。</li>
                                </ul>
                                <ul class="rule">
                                    <li>於12/01-12/10期間取消行程，收取20% 之賠償費用。</li>
                                    <li>於11/21-11/30期間取消行程，收取10% 之賠償費用。</li>
                                    <li>於11/20前取消行程，收取5%之賠償費用。</li>
                                </ul>
                            </div>
                        </div>
                        <div class="explain">
                            <input type="checkbox" name="read" id="read" class="read"><label for="read">詳閱並同意取消預約規定</label>
                        </div>
                        <p class="cant">請先勾選同意取消預約規定</p>
                        <div class="calcCon gray">
                            <div class="calc">
                                <div class="calcBox">
                                    <div class="calcItem">
                                        <h4>行程日期</h4>
                                        <p><?= $o_date ?></p>
                                    </div>
                                    <div class="calcItem">
                                        <h4>取消日期</h4>
                                        <p><?= $now_date ?></p>
                                    </div>
                                    <hr>
                                    <div class="calcItem">
                                        <h4>付款金額</h4>
                                        <p><?= $total ?></p>
                                    </div>
                                    <div class="calcItem">
                                        <h4>收取費用</h4>
                                        <p><?php
                                            if($now_date<=$b41_date){
                                                $dollar = intval($totalNum)*0.05;
                                                $percent = " (5%)";
                                                $length = strlen($dollar);
                                            }else if($now_date<=$b31_date){
                                                $dollar = intval($totalNum)*0.1;
                                                $percent = " (10%)";
                                                $length = strlen($dollar);
                                            }else if($now_date<=$b21_date){
                                                $dollar = intval($totalNum)*0.2;
                                                $percent = " (20%)";
                                                $length = strlen($dollar);
                                            }else if($now_date<=$b2_date){
                                                $dollar = intval($totalNum)*0.3;
                                                $percent = " (30%)";
                                                $length = strlen($dollar);
                                            }else if($now_date<=$b1_date){
                                                $dollar = intval($totalNum)*0.5;
                                                $percent = " (50%)";
                                                $length = strlen($dollar);
                                            }else if($now_date==$o_date){
                                                $dollar = intval($totalNum)*1;
                                                $percent = " (100%)";
                                                $length = strlen($dollar);
                                            }
                                            if($length<=3){
                                                echo "NT$".$dollar.$percent;
                                            }else{
                                                echo "NT$".substr_replace($dollar,',', $length-3,0).$percent;
                                            }
                                        ?></p>
                                    </div>
                                    <hr>
                                    <div class="calcItem">
                                        <h4>退費金額</h4>
                                        <h3 class="money"><?php 
                                            $money = intval($totalNum)-$dollar;
                                            $m_length = strlen($money);
                                            if($m_length<=3){
                                                echo "NT$".$money;
                                            }else{
                                                echo "NT$".substr_replace($money,',', $length-3,0);
                                            }
                                        ?></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="final">
                                <p>感謝您的支持並預約我們的活動，若您有任何問題或建議，歡迎來信或致電，我們非常需要您的寶貴意見，期待與您再次相見。</p>
                                <button class="finalCancel disabled" data-orderNum="<?= $o_row['order_number'] ?>">取消預約</button>
                            </div>
                        </div>
                    </div>
            </section>
        </main>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>
        var calcCon = $(".calcCon")
        var read = $(".read")
        var finalCancel = $(".finalCancel")
        var cant = $(".cant")
        
        read.on("change",function(){
            if(read.prop("checked")){
                calcCon.removeClass("gray")
                finalCancel.removeClass("disabled")
                cant.hide()
            }else{
                calcCon.addClass("gray")
                finalCancel.addClass("disabled")
            }
        })

        finalCancel.on("click",function(){
            var orderNum = $(this).attr('data-orderNum');
            if($(this).hasClass("disabled")){
                cant.show()
            }else{
                $.post('php_api/order_cancel_api.php',{'orderNum':orderNum},function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        setTimeout(function(){
                            location.href = 'member-travelManage.php'
                        },1000)
                    }
                },"JSON")
            }
        })
    </script>
</body>
</html>