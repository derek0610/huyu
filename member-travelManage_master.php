<?php
require __DIR__. '/php_api/__db_connect.php';

if((!isset($_SESSION['user'])) or ($_SESSION['user']['sid']!==1)){
    header('Location: index.php');
    exit;
}
    
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<?php require __DIR__.'/__html_js.php'?>
<script src="js/vue.js"></script>
<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
            <h1 class="title">後台管理</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn select"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
                <a href="member-travelManage_master2.php" class="memberBtn "><i class="far fa-comment-dots"></i> 客服信箱</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn select"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- -------------------------------------travelManage------------------------------------------ -->
        <section class="travelManage" id="ord_vue">
            <div class="order" v-for="ord in ords" v-bind:class="[ord.order_status=='已取消' ? 'gray' : '']">
                <form class="orderInfo" name="orderInfo" action="" method="post" >
                    <div class="orderNum"><span class="num">訂單編號：{{ord.order_number}}</span></div>
                    <div class="orderInfo">
                        <div class="orderTitle masterbgc">
                            <h3 v-if="ord.type=='walk'">浴衣體驗-散步方案</h3>
                            <h3 v-if="ord.type=='photo'">浴衣體驗-攝影方案</h3>
                            <h3 v-if="ord.type=='explore'">浴衣小旅行-探險路線</h3>
                            <h3 v-if="ord.type=='history'">浴衣小旅行-軼聞路線</h3>
                            <h6 v-if="ord.modify==1">修改時間：{{ord.modify_time}}</h6>
                            <div class="orderAct" v-bind:data-orderNum="ord.order_number">
                                <input type="button" class="payChange" value="變更狀態">
                                <input type="submit" class="payChangeYes" value="完成變更" style="display: none" >
                            </div>                                              
                        </div>
                        <div class="orderDetail">
                            <div class="basicInfo">
                                <div class="term name"><h5>會員名稱</h5><p>{{ord.name}}</p></div>
                                <div class="term name"><h5>手機</h5><p>{{ord.mobile}}</p></div>
                                <div class="term name"><h5>Email</h5><p>{{ord.email}}</p></div>
                            </div><hr>
                            <div class="basicInfo">    
                                <div class="term date"><h5>日期</h5><p>{{ord.date}}</p></div>
                                <div class="term time"><h5>時間</h5><p v-if="ord.time=='am'">上午 10:00</p><p v-else>下午 14:00</div>
                                <div class="term people"><h5>人數</h5><p>{{ord.people}}</p></div>
                                <div class="term plan">
                                    <h5>方案</h5>
                                    <p v-if="ord.plan=='inside'">棚內拍攝/每人NT$1,000</p>
                                    <p v-if="ord.plan=='outside'">外景拍攝/每人NT$1,600</p>
                                    <p v-if="ord.plan=='in_out'">棚拍+外拍/每人NT$2,000</p>
                                    <p v-if="ord.plan=='two'">兩小時體驗/每人NT$500</p>
                                    <p v-if="ord.plan=='four'">四小時體驗/每人NT$700</p>
                                    <p v-if="ord.plan=='six'">六小時體驗/每人NT$850</p>
                                    <p v-if="ord.plan=='twoFour'">二至四人/每人NT$2,000</p>
                                    <p v-if="ord.plan=='fiveNine'">五至九人/每人NT$1,800</p>
                                    <p v-if="ord.plan=='tenUp'">十人以上/每人NT$1,500</p>
                                </div>
                            </div><hr>
                            <div class="basicInfo" v-if="ord.extra>0">
                                <div class="term add" v-if="ord.extra>0"><h5>妝髮</h5><p>{{ord.extra}}人/每人 NT$500</p></div>
                            </div><hr v-if="ord.extra>0">
                            <div class="paymentInfo">
                                <div class="term total"><h5>付款金額</h5><p>{{ord.total}}</p></div>
                                <div class="term travelStatus"><h5>付款方式</h5><p>{{ord.pay_method}}</p></div>
                                <div class="term paymentStatus">
                                    <h5>付款狀態</h5>
                                    <p class="pStatus" v-bind:class="[ord.pay_status=='已付款' ? 'yes' : 'no']"  >{{ord.pay_status}}</p>
                                    <select class="form-control" id="paySelect" name="paySelect" style="display: none">
                                        <option value="待付款">待付款</option>
                                        <option value="已付款">已付款</option>
                                        <option value="未付款">未付款</option>
                                    </select>                                    
                                </div> 
                                <div class="term travelStatus" >
                                    <h5>行程狀態</h5>
                                    <p class="oStatus" v-bind:class="[ord.order_status=='已完成' ? 'already' : 'notYet']" >{{ord.order_status}}</p>
                                    <select class="form-control" id="orderSelect" name="orderSelect" style="display: none">
                                        <option value="尚未完成">尚未完成</option>
                                        <option value="已完成" >已完成</option>
                                        <option value="已取消" >已取消</option>
                                    </select>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="payInfo orderDetail" v-if="ord.pay_status=='待付款'">
                        <div class="basicInfo">
                            <div class="term"><h5>銀行代碼</h5><p>013國泰世華銀行</p></div>
                            <div class="term"><h5>轉帳帳號</h5><p>{{ord.atm_account}}</p></div>
                        </div>
                        <div class="basicInfo">
                            <div class="term"><h5>轉帳金額</h5><p>{{ord.total}}</p></div>
                            <div class="term"><h5>繳款期限</h5><p>{{ord.deadline}}</p></div>
                        </div>
                    </div>
                    <div class="orderFoot masterbgc"></div>
                </form>
            </div>
        </section>
    </div>
<!-- -------------------------------------script-------------------------------------------->
    <script>
        var vm=new Vue({
            el: "#ord_vue",
            data: {
                ords: []
            },
            ready: function(){
                $.ajax({
                    url: "php_api/order_master_api.php",
                    type: "POST",
                    cache:false,
                    dataType: 'json',
                    success: function(res){
                        vm.ords=res;
                    }
                });
            },
        });
    </script>
    <script>

        $("html").on("click",".payChange",function(){
            var thisOrder=$(this).parentsUntil(".order")
            $(this).hide().next().show();
            thisOrder.find(".form-control").show().prev().hide();
            var payStatus=thisOrder.find(".pStatus").text();
            switch(payStatus){
                case "待付款": thisOrder.find("#paySelect option:nth-child(1)").attr("selected",true); break;
                case "已付款": thisOrder.find("#paySelect option:nth-child(2)").attr("selected",true); break;
                case "未付款": thisOrder.find("#paySelect option:nth-child(3)").attr("selected",true); break;
            }
            var orderStatus=thisOrder.find(".oStatus").text();
            switch(orderStatus){
                case "尚未完成": thisOrder.find("#orderSelect option:nth-child(1)").attr("selected",true); break;
                case "已完成": thisOrder.find("#orderSelect option:nth-child(2)").attr("selected",true); break;
                case "已取消": thisOrder.find("#orderSelect option:nth-child(3)").attr("selected",true); break;
            }
        });

        $("html").on("click",".payChangeYes",function(){
            var thisOrder=$(this).parentsUntil(".order")
            $(this).hide().prev().show();
            thisOrder.find(".form-control").hide().prev().show();
            var orderNum = $(this).parent().attr("data-orderNum");
            var newPayStatus=thisOrder.find("#paySelect option:selected").text();
            var newOrderStatus=thisOrder.find("#orderSelect option:selected").text();
            console.log($(this).parentsUntil(".order").serialize()+"&orderNum="+orderNum)
            $.post('php_api/order_master_edit_api.php', $(this).parentsUntil(".order").serialize()+"&orderNum="+orderNum, function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(500).fadeOut();
                        setTimeout(function(){location.reload()},1000)
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(1000).fadeOut();
                    }
                }, "json")
            
        });

        // function editInfo(){
            
        // }

    </script>
</body>
</html>