<?php require __DIR__.'/php_api/__db_connect.php';?>

<?php require __DIR__.'/__html_head.php'?>
    
    <title>忽浴 | 相冊衣覽-客漾寫真</title>
    <link rel="stylesheet" href="css/album-guest.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

<?php $album = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <header>
            <h1 class="title">客樣寫真</h1>
            <p class="intro">「忽浴」提供豐富的體驗方式讓你身穿日式浴衣悠遊北投。如果想來場深度之旅，「軼聞路線」或「探索路線」將有專業的導覽人員帶領你認識不一樣的北投。</p>
        </header>
        <div class="buttonCon">
            <p>每次刷新都可以看到不一樣的照片喔! <i class="fas fa-hand-point-right"></i></p>
            <button class="newPhoto">刷新照片</button>
        </div>
        <div>
            <div class="gridcon">
                <a class="a bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="a1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b3 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b4 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b5 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b6 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="c bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="c1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="d bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="d1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="e bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="f bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="g bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="h bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="i bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="j bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="k bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="l bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
            </div>

            <div class="gridcon">
                <a class="a bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="a1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b3 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b4 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b5 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="b6 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="c bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="c1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="d bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="d1 bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="e bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="f bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="g bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="h bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="i bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="j bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="k bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
                <a class="l bdra" data-fancybox="gallery"><img alt="" class="Imgctl"></a>
            </div>
        </div>
        <!-- ------------------------------------------------------------------------------- -->
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script>
        
        var ar=[]
        for(let i=1;i<69;i++){ar.push(i)};

        function randomSort(a, b) {
            return Math.random() > 0.5 ? -1 : 1;
        }

        var br = ar.sort(randomSort)
        

        $(".gridcon").each(function(){
            var childrenNum = $(this).children().length
            var gridconNum = $(this).index()*childrenNum
            for(let i=0;i<childrenNum;i++){
                $(this).children().eq(i).addClass("r"+(gridconNum+i+1));
                console.log(gridconNum+i+1)
            }
        })

        var rNum = $(".bdra").length
        for(let i=1;i<rNum+1;i++){
            var r = $(".r"+i)
            var num = br.pop()
            r.attr('href','images/album-guest/'+num+'.jpg').children().attr('src','images/album-guest/'+num+'.jpg');
        }

        $(window).on("scroll resize",function(){
            $(".bdra").each(function(){
                var windowScroll = $(window).scrollTop()
                var windowHeight = $(window).innerHeight()
                var rTop = $(this).offset().top-windowScroll
                if(rTop<windowHeight+350){
                    $(this).css("opacity",1)
                }
                if( rTop>windowHeight){
                    $(this).css("opacity",0)
                }
            })
        })

        $(".newPhoto").click(function(){
            location.href = 'album-guest.php'
        })
    </script>
</body>

</html>