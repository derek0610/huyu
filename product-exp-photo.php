<?php
require __DIR__. '/php_api/__db_connect.php';

?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 浴衣漫遊-攝影方案</title>
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="css/product.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

        <section class="photoSlider">
            <div class="wave"></div> 
            <div class="photo-group">
                <img src="images/Portfolio8.jpg" alt="">
                <img src="images/Portfolio7.jpg" alt="">
                <img src="images/linda5.png" alt="">
                <img src="images/linda4.png" alt="">
                <img src="images/children3.jpg" alt="">
                <img src="images/children4.jpg" alt="">
                <img src="images/hair.jpg" alt="">
            </div>
        </section>
        <main>
            <header>
                <h1 class="title">浴衣體驗-攝影方案</h1>
                <h3>紀錄美好時光，永久保存難忘的回憶。</h3>
                <p class="intro">你有想過在每一趟旅程中，除了回憶之外還能留下什麼嗎?難道每次旅行都只能拍「觀光客」風格的照片嗎?，讓「忽浴」的專業攝影師幫你紀錄旅行中每個美麗的瞬間，用影像記錄你的生活吧!</p>
                <div class="act">
                    <ul class="info">
                        <li><img src="images/element/icon_clock.svg" alt="">2.5小時</li>
                        <li><img src="images/element/icon_avatar.svg" alt="">1人成行</li>
                        <li><img src="images/element/icon_globe.svg" alt="">中文服務</li>
                    </ul>
                    <button class="reserve">馬上預約</button>
                    <ul class="share">
                        <li><img src="images/element/icon_fb.svg" alt=""></li>
                        <li><img src="images/element/icon_twitter.svg" alt=""></li>
                        <li><img src="images/element/icon_line.svg" alt=""></li>
                    </ul>
                </div>
            </header>
            <ul class="product-nav">
                <li class="special p-active">活動特色</li>
                <li class="detail">詳細內容</li>
                <li class="comment">旅客評論</li>
            </ul>
            <article class="product-content">
                <ul class="content special show">
                    <!-- <video src="video/YUKATA.mp4" controls autoplay muted></video> -->
                    <li class="group">
                        <h3 class="special-title">專業拍攝團隊</h3>
                        <p class="special-content">「忽浴」有北投最專業的攝影團隊，無論鏡位、燈光、後製皆是雜誌拍攝等級。我們有精心布置的攝影場景;我們帶你到沒有觀光客的秘境，所有的用心都是為了記錄每個美好回憶，到我們的 <a href="album-guest.php"><i class="fas fa-hand-point-right"></i>「相冊衣覽-客樣寫真」</a>看看其他顧客的美好回憶吧!</p>
                    </li>
                    <li class="group">
                        <h3 class="special-title">日式浴衣體驗</h3>
                        <p class="special-content">「忽浴」提供完善的日式浴衣換裝體驗，讓漫步在北投溫泉街的您，不用遠赴日本也能穿著正統日式浴衣，用不同方式感受北投的人文風情，我們提供的和服和浴衣款式齊全多樣，每個人都能挑到自己喜歡的花樣，最棒的是還包含免費的配件和髮飾。你也可以先到 <a href="album-clothes.php"><i class="fas fa-hand-point-right"></i>「相冊衣覽-著物展示」</a>找找喜歡的浴衣款式喔!</p>
                    </li> 
                    <li class="group">
                        <h3 class="special-title">專屬妝髮設計</h3>
                        <p class="special-content">「忽浴」團隊有專業的髮型師及設計師，除了在現場根據你的身材及膚色搭配和服的小技巧外，你也可以額外加購我們的妝髮設計服務，四款指定髮型以及標準的和風妝容，讓你美美的走在北投溫泉街上，就像身處京都的古道一般優雅。</p>
                    </li>
                    <li class="group">
                        <h3 class="special-title">精美伴手禮</h3>
                        <p class="special-content">享受完一場文化洗禮怎麼能不帶點特別的回家呢，現在只要在旅程的最後幫我們在網站上留下你珍貴的意見，我們就送給每一位團員北投當地的精美小禮物喔！</p>
                    </li>
                </ul>
                <ul class="content detail ">
                    <li class="group">
                        <h3 class="detail-title">費用包含</h3>
                        <ul class="detail-content">
                            <li>全套浴衣、配件搭配及換裝（現場挑選）。</li>
                            <li>專業攝影師及攝影助理拍攝指導。</li>
                            <li>根據方案拍攝，並提供專業修圖（顧客自選十張）。</li>
                            <li>旅遊平安險、公共意外險。</li>
                            <li>現場線上評論，每位團員均可獲精美紀念品。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">方案說明</h3>
                        <ul class="detail-content">
                            <li>棚內拍攝：三組棚內場景拍攝，每組場景十張影像，共三十張。</li>
                            <li>外景拍攝：三組新北投景點拍攝，每組場景十張影像，共三十張。</li>
                            <li>棚拍+外拍：兩組拍內場景+兩組新北投景點拍攝，每組場景十張影像，共四十張。</li><br/>
                            <li>每一方案均提供專業修圖（顧客自選十張）。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">妝髮加購</h3>
                        <ul class="detail-content">
                            <li>根據您的膚色臉型設計妝容。</li>
                            <li>四款指定髮型挑選一款。</li>
                            <li>專業妝髮師梳畫。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">流程概述</h3>
                        <ul class="detail-content">
                            <li>提前15分鐘到店。</li>
                            <li>選擇和服樣式（10分鐘）。</li>
                            <li>穿戴（15分鐘）。</li>
                            <li>拍攝行程（120分鐘）</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">溫馨提醒</h3>
                        <ul class="detail-content">
                            <li>夏季建議穿薄內衣，背心。</li>
                            <li>冬季建議穿防寒內衣或輕薄夾克（低領）、緊身褲或保暖褲（襪套除外）來禦寒。</li>
                            <li>注意不要穿有領的襯衫，容易乾擾和服的展示。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">注意事項</h3>
                        <ul class="detail-content">
                            <li>浴衣款式依現場提供為主，無提供事先預訂服務。</li>
                            <li>當天請於行程出發前15分鐘至「忽浴」櫃檯報到，如未依規定時間到達，「忽浴」有權利調整行程內容。</li>
                            <li>行程當天請攜帶證件，並抵押至櫃台，行程結束將全數歸還。</li>
                            <li>活動全程均有保險，請提供完整旅客資訊以利辦理。</li>
                            <li>浴衣及相關配件如有無法恢復之污漬、破損、遺失，將以現金收取賠償費用。</li>
                            <li>如未依規定時間歸還浴衣，每延遲30分鐘，加收現金100元租金。</li>
                            <li>0-2歲兒童如有攜帶證件，可免費參與本活動。</li>
                            <li>兒童浴衣，僅適合身高100公分至140公分之兒童穿著。</li>
                            <li>孕婦無法穿著浴衣，若參與行程將不另外退費。</li>
                            <li>我們提供行李寄放服務，貴重物品請隨身攜帶，「忽浴」不負保管責任。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">取消與延後</h3>
                        <ul class="detail-content">
                            <li>出發前41天以前取消，將收取5%之費用。</li>
                            <li>出發前31至40天取消，將收取10%之費用。</li>
                            <li>出發前21至30天取消，將收取20%之費用。</li>
                            <li>出發前2至20天取消，將收取30%之費用。</li>
                            <li>出發前1天取消，將收取50%之費用。</li>
                            <li>出發當天取消、無故未到者，恕不退費。</li>
                            <li>72小時(三日)內將無法更改、延後預約內容及旅客資訊</li>
                            <li>每筆預約僅提供延後服務一次，請謹慎使用。</li>
                        </ul>
                    </li>
                </ul>
                <ul class="content comment ">
                    <button class="comment-btn">撰寫評論</button>
                    <form class="comment-form" action="" name="walk-comment" method="post" onsubmit="return text()">
                        <fieldset class="rating-group">
                            <label for="s1" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s1" class="radio" value="1">

                            <label for="s2" class="rating-star"><i class="fas fa-star no-rate rating"></i></label> 
                            <input type="radio" name="star" id="s2" class="radio" value="2">
                        
                            <label for="s3" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s3" class="radio" value="3">
                          
                            <label for="s4" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s4" class="radio" value="4">
                          
                            <label for="s5" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s5" class="radio" value="5">
                            <span style="float:right " class="check check1">請選擇星等</span>
                        </fieldset>
                        <p class="check check2">請使用至少20個字說明</p>
                        <textarea name="comment" id="comment-text" class="comment-text" placeholder="請使用至少20個字說說你的親身體驗" onkeyup="autoGrow(this);" rows="3"></textarea><br/>
                        <div class="submit-group">
                            <button class="submit">送出評論</button>
                        </div>
                    </form>
                    <div class="commentCon">
                
                    </div>
                    <div class="pageCon">

                    </div>
                </ul>
            </article>
        </main>
        <div class="reserve-box">
            <div class="close"><div class="close-symbol"><i class="fas fa-times"></i></div></div>
            <form action="" class="reserve-form" name="walk-reserve" method="post" onsubmit="return pay()">
                <input type="hidden" name="type" value="photo">
                <input id="datetimepicker" type="text" name="date" value="<?= date("Y-m-d",strtotime('+4 day')) ?>">
                <div class="time-select">
                    <label for="time" class="time-label">時間</label>
                    <select name="time" id="time">
                        <option value="am">上午 10:00</option>
                        <option value="pm">下午 14:00</option>
                    </select>
                </div>
                <fieldset class="plan-select">
                    <legend class="plan-select-title">選擇方案</legend>
                        <input type="radio" id="inside" name="plan" class="plan-radio" value="inside" data-price="1000" checked>
                        <label for="inside" class="plan"><h3>棚內拍攝</h3><h3 class="price"><span>每人</span>NT$1,000</h3></label>

                        <input type="radio" id="outside" name="plan" class="plan-radio" value="outside" data-price="1600">
                        <label for="outside" class="plan"><h3>外景拍攝</h3><h3 class="price"><span>每人</span>NT$1,600</h3></label>

                        <input type="radio" id="in_out" name="plan" class="plan-radio" value="in_out" data-price="2000">
                        <label for="in_out" class="plan"><h3>棚拍+外拍</h3><h3 class="price"><span>每人</span>NT$2,000</h3></label>
                </fieldset>
                <div class="people-select">
                    <label for="people" class="people-label">人數</label>
                    <div class="less">－</div>
                    <input type="text" id="people" name="people" value="1" readonly>
                    <div class="add">＋</div>
                </div>
                <hr>
                <h3 style="color: #fff">妝髮服務 NT$500</h3>
                <div class="makeup-select">
                    <label for="makeup" class="makeup-label">加購妝髮</label>
                    <div class="makeup-less">－</div>
                    <input type="text" id="makeup" name="makeup" value="0" data-price="500" readonly>
                    <div class="makeup-add">＋</div>
                </div>
                <hr>
                <div class="final">
                    <input type="text" name="total" id="total" value="NT$1,000" readonly>
                    <input type="submit" class="pay" value="預約結帳">
                </div>
            </form>
        </div>

<?php require __DIR__.'/__html_js.php'?>

    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script src="js/jquery.datetimepicker.full.min.js"></script>
    <script src="js/lodash.js"></script>
    <script src="js/product-exp.js"></script>
    <script>

        var params = {
            'page': 1,
            'type': "photo"
        };

        <?php if(isset($_SESSION['tpOrder']) && $_SESSION['tpOrder']['type']=="photo"):?>
            $('#datetimepicker').val("<?= $_SESSION['tpOrder']['date'] ?>");
            $('#datetimepicker').datetimepicker({
                value: "<?= $_SESSION['tpOrder']['date'] ?>",
            });
            $('#time option[value="<?= $_SESSION['tpOrder']['time'] ?>"]').prop("selected",true);
            $('.plan-radio[value="<?= $_SESSION['tpOrder']['plan'] ?>"]').prop("checked",true);
            $('#people').val(<?= $_SESSION['tpOrder']['people'] ?>);
            $('#makeup').val(<?= $_SESSION['tpOrder']['makeup'] ?>);
            $('#total').val("<?= $_SESSION['tpOrder']['total'] ?>");
        <?php endif;?>
    
        function pay() {
            $.post('php_api/tp_order_api.php',$('.reserve-form').serialize(),function(data){
                console.log(data)

                if(data.code==444){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                }
                if(data.code==555){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                    $(".bgBlur").fadeIn();
                }
                if(data.code==1){
                    location.href = 'order-enter.php'
                }       
            },"json")
            return false;
        }

        // -------------------------------------------comment----------------------------------------        
        var order_number
        commentBtn.click(function(){
            $.post('php_api/comment_api.php',{type:"photo"},function(data){
                if(data.code==444){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                    $(".bgBlur").fadeIn();
                }else if(data.code==000){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                }else if(data.code==111){
                    $(".comment-form").slideToggle();
                    order_number = data.orderNum
                }
            },"JSON")
        });

        var star = $('.radio');
        var starNum = $('.radio').length
        var starInput = false
        var starCheck = $(".check1")
        var comment = $(".comment-text")
        var commentCheck = $(".check2")
        var commentFields = [starCheck, commentCheck];

        function text(){
            commentFields.forEach(function(val){
                val.hide();
            });

            var isPass = true;

            for(var i=0;i<starNum;i++){ 
                if(star[i].checked) 
                starInput=true; 
            }
            if(!starInput) {
                isPass = false;
                starCheck.show()
            }
            if(comment.val().length<20){
                isPass = false;
                commentCheck.show()
            }
            console.log(isPass)
            if(isPass){
                var commentData = {
                    "order_number": order_number,
                    "star": $('.radio:checked').val(),
                    "text": comment.val()
                }

                $.post('php_api/comment_insert_api.php',commentData, function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        $(".comment-form").slideToggle();
                        reload({})
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(800).fadeOut();
                    }
                }, "json")
            }
            return false
        }

        var commentCon = $(".commentCon")
        var comment_str=`
            <li class="group">
                <div class="comment-title">
                    <span class="user"><%= name %></span>
                    <span class="star">
                        <%= starHtml %>
                    </span>
                    <span class="date"><%= time %></span> 
                </div>
                <p class="comment-content"><%= text %></p>
            </li>`;
        var comment_template = _.template(comment_str);

        var pageCon = $(".pageCon")
        var pageCon_str=`<a class="commentPage <%= page==i ? 'active' : '' %>" href="javascript:reload({page:<%= i %>})"><%= i %></a>`
        var pageCon_template = _.template(pageCon_str);

        function reload(obj){
            if(obj.page!==undefined){
                params.page = obj.page;
            }

            $.post('php_api/comment_show_api.php',params,function(data){
                var page = data.page;
                var totalPages = data.totalPages;

                pageCon.html("");
                for(var i=1;i<=totalPages;i++){
                    pageCon.append(pageCon_template({i:i, page:page}))
                }

                commentCon.html("");
                for(var i=0; i<data.rows.length; i++){
                    var name = data.rows[i].name
                    var time = data.rows[i].time
                    var text = data.rows[i].text
                    var star = data.rows[i].star
                    var starHtml = ''
                    for(let i=0;i<star;i++){
                        starHtml += '<i class="fas fa-star"></i>'
                    }
                    for(let i=0;i<5-star;i++){
                        starHtml += '<i class="fas fa-star no-rate"></i>'
                    }
                    var comment_array={
                        "name":name,
                        "time":time,
                        "text":text,
                        "starHtml":starHtml,
                    }
                    commentCon.append(comment_template(comment_array));
                }
            },"JSON")
        }

        reload({})
    </script>
</body>
</html>