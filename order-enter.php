<?php 
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user']) || !isset($_SESSION['tpOrder'])){
    header('Location: index.php');
    exit;
}

require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 預約結帳</title>
    <link rel="stylesheet" href="css/order-enter.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <div class="totalBoxB">
            <p class="infoTag">總金額</p><h3 class="orderTotal"><?= $_SESSION['tpOrder']['total'] ?></h3>
        </div>
        <aside>
            <div class="orderCon">
                <div class="infoBox">
                    <h3 class="orderName">
                        <?php 
                        switch($_SESSION['tpOrder']['type']){
                            case "walk":echo "浴衣體驗-散步方案";break;
                            case "photo":echo "浴衣體驗-攝影方案";break;
                            case "explore":echo "浴衣小旅行-探險路線";break;
                            case "history":echo "浴衣小旅行-軼聞路線";break; 
                        }?>
                    </h3><hr>
                    <ul class="orderInfo">
                        <li><p class="infoTag">日期</p><p class="infoText"><?= $_SESSION['tpOrder']['date'] ?></p></li>
                        <li><p class="infoTag">時間</p><p class="infoText"><?= $_SESSION['tpOrder']['time']=="am" ? "上午 10:00" : "下午 14:00" ?></p></li>
                        <li><p class="infoTag">人數</p><p class="infoText"><?= $_SESSION['tpOrder']['people'] ?>人</p></li>
                    </ul><hr>
                    <ul class="orderInfo">
                        <li><p class="infoTag">方案</p><p class="infoText">
                            <?php 
                            switch($_SESSION['tpOrder']['plan']){
                                case "inside":echo "棚內拍攝/每人NT$1,000";break;
                                case "outside":echo "外景拍攝/每人NT$1,600";break;
                                case "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                case "two":echo "兩小時體驗/每人NT$500";break;
                                case "four":echo "四小時體驗/每人NT$700";break;
                                case "six":echo "六小時體驗/每人NT$850";break;
                                case "twoFour":echo "二至四人/每人NT$2,000";break;
                                case "fiveNine":echo "五至九人/每人NT$1,800";break;
                                case "tenUp":echo "十人以上/每人NT$1,500";break;
                            }?>
                        </p></li>
                        <?php if($_SESSION['tpOrder']['makeup']>0):?>
                        <li><p class="infoTag">妝髮</p><p class="infoText"><?= $_SESSION['tpOrder']['makeup'] ?>人/每人 NT$500</p></li>
                        <?php endif;?>
                    </ul>
                </div>
                <div class="totalBox">
                    <p class="infoTag">總金額</p><h3 class="orderTotal"><?= $_SESSION['tpOrder']['total'] ?></h3>
                </div>
            </div>
        </aside>
        <section class="bigForm">
            <div class="traveler">
                <div class="stepTittle">
                    <h3 class="stepNum">1</h3><h2 class="stepText">旅客資料</h2>
                </div>
                <div class="stepCon">
                    <div class="mainTraveler">
                        <div class="travelerTitle"><h3>團員1(主要聯絡人)</h3></div>
                        <div class="explain"><p>為申請旅遊平安險，請提供完整資訊，所有資訊均可於行程出發日72小時以前進行線上修改。</p></div>
                        <form action="" method="post" name="main" class="orderForm mainStaff">
                            <div class="itemGroup">
                                <div class="formItem">
                                    <label for="mainName"><h5>真實姓名</h5></label>
                                    <small class="wrong">請輸入真實姓名</small>
                                    <input type="text" name="name" id="mainName" class="textInput" placeholder="請輸入真實姓名" value="<?= $_SESSION['user']['name'] ?>">
                                </div>
                                <div class="formItem">
                                    <label for="mainID"><h5>身分證字號</h5></label>
                                    <small class="wrong">請輸入正確的身分證字號</small>
                                    <input type="text" name="id" id="mainID" class="textInput" placeholder="A120000000">
                                </div>
                                <div class="formItem">
                                    <label for="mainMobile"><h5>行動電話</h5></label>
                                    <small class="wrong">請輸入正確的電話號碼格式</small>
                                    <input type="text" name="mobile" id="mainMobile" class="textInput" placeholder="0900-000-000" value="<?= $_SESSION['user']['mobile'] ?>">
                                </div>
                            </div>
                            <div class="formItem">
                                <label for="mainEmail"><h5>電子信箱</h5></label>
                                <small class="wrong">請輸入正確的電子信箱</small>
                                <input type="text" name="email" id="mainEmail" class="textInput" placeholder="請輸入正確的電子信箱" value="<?= $_SESSION['user']['email'] ?>">
                            </div>
                        </form>
                    </div>
                    <?php if($_SESSION['tpOrder']['people']>1): ?>
                    <div class="otherTraveler">
                        <div class="travelerTitle"><h3>同行團員</h3></div>
                        <div class="explain"><p>為申請旅遊平安險，請提供完整資訊，所有資訊均可於行程出發日72小時以前進行線上修改。</p></div>
                        <div class="orderForm">
                            <?php for($i=1;$i<$_SESSION['tpOrder']['people'];$i++):?>
                            <form action="" method="post" name="other" class="staff">
                                <h3>團員<?= $i+1 ?></h3>
                                <div class="itemGroup">
                                    <div class="formItem">
                                        <label for="other<?= $i ?>Name"><h5>真實姓名</h5></label>
                                        <small class="wrong">請輸入真實姓名</small>
                                        <input type="text" name="name" id="other<?= $i ?>Name" class="textInput" placeholder="請輸入真實姓名" >
                                    </div>
                                    <div class="formItem"> 
                                        <label for="other<?= $i ?>ID"><h5>身分證字號</h5></label>
                                        <small class="wrong">請輸入正確的身分證字號</small>
                                        <input type="text" name="id" id="other<?= $i ?>ID" class="textInput" placeholder="A120000000">
                                    </div>
                                    <input type="hidden" name="mobile" id="other<?= $i ?>mobile" class="textInput" value="">
                                    <input type="hidden" name="email" id="other<?= $i ?>email" class="textInput" value="">
                                </div>
                            </form>
                        <?php endfor;?>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="payment">
                <div class="stepTittle">
                    <h3 class="stepNum">2</h3><h2 class="stepText">付款方式</h2>
                </div>
                <div class="stepCon">
                    <div class="explain"><p><span><i class="fas fa-lock"></i></span>所有付款資訊皆已安全加密保護。</p></div>
                    <div class="paySelect">
                        <div>
                            <input type="radio" name="payment" id="atm" value="atm" checked>
                            <label for="atm">ATM轉帳</label>
                        </div>
                        <div> 
                            <input type="radio" name="payment" id="card" value="card">
                            <label for="card">信用卡<img src="images/card.png" alt="" width="120"></label>
                        </div>
                    </div>
                    <div class="explain bg-white" id="atmBox"><p>確認結帳後我們將提供您轉帳資料，並請您在指定時間內進行轉帳付款，即完成預約。</p></div>
                    <div class="orderForm" id="cardBox" style="display: none">
                        <div class="itemGroup">
                            <div class="formItem">
                                <label for="cardNum"><h5>卡號</h5></label>
                                <input type="text" name="cardNum" id="cardNum" class="textInput" placeholder="請輸入您的信用卡號" >
                            </div>
                            <div class="formItem">
                                <label for=""><h5>有效日期</h5></label>
                                <select name="cardMonth" id="cardMonth" class="selectInput">
                                    <option selected disabled hidden>月</option>
                                </select>
                                <select name="cardYear" id="cardYear" class="selectInput">
                                    <option selected disabled hidden>年</option>
                                </select>
                            </div>
                            <div class="formItem"> 
                                <label for="cardCVV"><h5>安全碼</h5></label>
                                <input type="text" name="cardCVV" id="cardCVV" class="textInput" placeholder="CVV碼">
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="payCon">
                    <div class="confirmPay">
                        <p>點擊「確認付款」，即表示您已確認訂單無誤以及同意右邊顯示的總金額，且同意<strong class="red">預約取消/延後規則</strong>。</p>
                        <button class="pay">確認結帳</button>
                    </div>
                    <div class="explain flex"><p>有任何疑問歡迎聯繫我們！</p><p>門市電話：02-2369-3366</p><p>服務時間：AM09:30-PM18:30</p></div>
                    <div class="explain border bg-red"><h3 class="white">預約取消/延後規則</h3></div>
                    <ul class="explain bg-white">
                        <li>出發前41天以前取消，將收取5%之費用。</li>
                        <li>出發前31至40天取消，將收取10%之費用。</li>
                        <li>出發前21至30天取消，將收取20%之費用。</li>
                        <li>出發前2至20天取消，將收取30%之費用。</li>
                        <li>出發前1天取消，將收取50%之費用。</li>
                        <li>出發當天取消、無故未到者，恕不退費。</li>
                        <li>72小時(三日)內將無法更改、延後預約內容及旅客資訊</li>
                        <li>每筆預約僅提供延後服務一次，請謹慎使用。</li>
                    </ul>
                </div>
            </div>
        </section>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>
        var windowWidth = $(window).outerWidth()
        $(window).resize(function(){
            windowWidth = $(window).outerWidth()
        })

        for(var i=0;i<20;i++){
            var date = new Date
            var year = $('<option></option>')
            var thisYear = date.getFullYear()
            year.val(thisYear+i)
            year.text(thisYear+i)
            $("#cardYear").append(year)
        }
        for(var i=1;i<13;i++){
            var month = $('<option></option>')
            month.val(i)
            month.text(i)
            $("#cardMonth").append(month)
        }

        var payMethod = $(".paySelect input")
        payMethod.change(function(){
            if($("#card").prop("checked")){
                $("#cardBox").show()
                $("#atmBox").hide()
            }else{
                $("#cardBox").hide()
                $("#atmBox").show()
            }
        }) 

        function  windowScroll() {
            var windowWidth = $(window).outerWidth()
            if(windowWidth>1024){
                $(".totalBoxB").css("transform","translate(0,100%)")
                $(window).off("scroll")
                $(window).on("scroll",function(){
                    var windowScrollTop = $(window).scrollTop()
                    $(".orderCon").offset({top:conTop+windowScrollTop})
                })
            }else if(windowWidth<1024){
                $(".orderCon").offset({top:conTop})
                $(window).off("scroll")
                $(window).on("scroll",function(){
                    var bigFormTop = $(".bigForm").offset().top-$(window).scrollTop()
                    if(bigFormTop<60){
                        $(".totalBoxB").css("transform","translate(0,0)")
                    }else if(bigFormTop>60){
                        $(".totalBoxB").css("transform","translate(0,100%)")
                    }
                })
            }
        }
        windowScroll()
        var conTop = $(".orderCon").offset().top
        $(window).resize(function(){
            windowScroll()
        })

        $(".wrong").hide()

        $(".pay").click(function(){
            var $name = $('input[name="name"]')
            var $id = $('input[name="id"]')
            var $mobile = $('input[name="mobile"]')
            var $email = $('input[name="email"]')
            var fields = [$name, $id, $mobile, $email];
            var mobileRegex = /^09\d{2}\-\d{3}\-\d{3}$/;
            var idRegex = /^[A-Z]\d{9}$/;

            fields.forEach(function(val){
                val.prev(".wrong").hide();
            });
            
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            var isPass = true;

            $name.each(function(){
                var nameVal = $(this).val()
                if(nameVal.length<2){
                    isPass = false;
                    $(this).prev(".wrong").show();
                }
            })

            $id.each(function(){
                var idVal = $(this).val()
                if(!idRegex.test(idVal)){
                    isPass = false;
                    $(this).prev(".wrong").show();
                }
            })

            if(!mobileRegex.test($mobile.val())){
                isPass = false;
                $mobile.prev(".wrong").show()
            }
            
            if(!validateEmail($email.val())){
                isPass = false;
                $email.prev(".wrong").show()
            }

            if(!isPass){
                $("html").animate({scrollTop:0},500);
            }else{
                var formData = {}
                var mainData = $(".mainStaff").serializeArray()
                var staffNum = $(".staff").length
                formData[0]=mainData
                for(var i=0;i<staffNum;i++){
                    var otherData = $(".staff").eq(i).serializeArray()
                    formData[i+1] = otherData
                }
                $.post('php_api/order_insert_api.php', formData, function(data){
                    if(data.success){
                        location.href = 'order-success.php'
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(800).fadeOut();
                    }
                }, "json")
            }
        })
    </script>
</body>
</html>