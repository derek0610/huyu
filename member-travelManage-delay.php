<?php
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
}
if(!isset($_GET['orderNum'])){
    header('Location: member-travelManage.php');
    exit;
}

$member_sid = $_SESSION['user']['sid'];
$order_number = $_GET['orderNum'];

$o_sql = "SELECT * FROM `orders` WHERE 1 AND `order_number`=$order_number AND `belong`=$member_sid";
$o_stmt = $pdo->query($o_sql);
$o_row = $o_stmt->fetch();

?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member-delay.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<!-- --------------------------------------header--------------------------------------------- -->
    <div class="container">
        <header id="app3" >
                <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- ---------------------------------travelManage-delay-------------------------------------- -->
        <main>
            <section class="travelManage">
                <div class="orderNum"><h2>延後預約</h2><span class="num">訂單編號：<?= $o_row['order_number'] ?></span></div>
                    <div class="orderInfo">
                        <div class="orderTitle">
                            <h3><?php 
                                switch($o_row['type']){
                                case "walk":echo "浴衣體驗-散步方案";break;
                                case "photo":echo "浴衣體驗-攝影方案";break;
                                case "explore":echo "浴衣小旅行-探險路線";break;
                                case "history":echo "浴衣小旅行-軼聞路線";break; 
                            }?></h3>
                        </div>
                        <div class="orderDetail">
                            <div class="basicInfo">
                                <div class="term date"><h5>日期</h5><p><?= $o_row['date'] ?></p></div>
                                <div class="term time"><h5>時間</h5><p><?= $o_row['time']=="am" ? "上午 10:00" : "下午 14:00" ?></p></div>
                                <div class="term people"><h5>人數</h5><p><?= $o_row['people'] ?>人</p></div>
                            </div>
                            <hr>
                            <div class="basicInfo">
                            <div class="term plan"><h5>方案</h5><p><?php 
                                switch($o_row['plan']){
                                    case  "inside":echo "棚內拍攝/每人NT$1,000";break;
                                    case  "outside":echo "外景拍攝/每人NT$1,600";break;
                                    case  "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                    case  "two":echo "兩小時體驗/每人NT$500";break;
                                    case  "four":echo "四小時體驗/每人NT$700";break;
                                    case  "six":echo "六小時體驗/每人NT$850";break;
                                    case  "twoFour":echo "二至四人/每人NT$2,000";break;
                                    case  "fiveNine":echo "五至九人/每人NT$1,800";break;
                                    case  "tenUp":echo "十人以上/每人NT$1,500";break;
                                }?></p></div>
                                <?php if($o_row['extra']>0):?>
                                <div class="term add"><h5>妝髮</h5><p><?= $o_row['extra'] ?>人/每人 NT$500</p></div>
                                <?php endif;?>
                            </div>
                            <hr>
                            <div class="basicInfo">
                                <div class="term total"><h5>付款金額</h5><p><?= $o_row['total'] ?></p></div>
                                <div class="term total"><h5>付款方式</h5><p><?= $o_row['pay_method'] ?></p></div>
                                <div class="term paymentStatus"><h5>付款狀態</h5><p class="yes"><?= $o_row['pay_status'] ?></p></div>
                                <div class="term travelStatus"><h5>行程狀態</h5><p class="notYet"><?= $o_row['order_status'] ?></p></div>
                            </div>
                        </div>
                        <div class="delayTitle">
                            <h3>延後預約規定</h3>
                        </div>
                        <div class="delayRule">
                            <p>我們能為您保留並延後預約一次，每筆預約僅能延後一次，請謹慎使用。<br>為保障其他旅客權益，若未來有再次延後之需求，請您取消預約後再次預定，如有不便，敬請見諒。</p>
                            <p class="st">若您曾將預約延後，又將此筆預約進行取消，我們收取的賠償費用將以 30% 為起跳，並依「取消預約規定」上調費用金額。</p>
                        </div>
                        <div class="explain">
                            <input type="checkbox" name="read" id="read" class="read"><label for="read">詳閱並同意延後預約規定</label>
                        </div>
                        <p class="cant">請先勾選同意延後預約規定</p>
                        <div class="pickCon gray">
                                <div class="pick">
                                        <div class="pickBox">
                                            <div class="pickItem">
                                                <h4>原行程日期</h4>
                                                <p class="before"><?= $o_row['date'] ?></p>
                                            </div>
                                            <div class="pickItem">
                                                <h4>欲延後日期</h4>
                                                <p class="after"></p>
                                            </div>
                                            <input type="text" name="" id="datetimepicker" value="<?= $o_row['date'] ?>">
                                        </div>
                                    </div>
                            <div class="final">
                                <p>感謝您的支持並預約我們的活動，若您有任何問題或建議，歡迎來信或致電，我們非常需要您的寶貴意見，期待與您相見。</p>
                                <button class="finalDelay disabled" data-orderNum="<?= $o_row['order_number'] ?>">延後預約</button>
                            </div>
                        </div>
                    </div>
            </section>
        </main>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script src="js/jquery.datetimepicker.full.min.js"></script>
    <script>
        var pickCon = $(".pickCon")
        var read = $(".read")
        var finalDelay = $(".finalDelay")
        var cant = $(".cant")

        if(read.prop("checked")){
            pickCon.removeClass("gray")
            finalDelay.removeClass("disabled")
            cant.hide()
            $(".xdsoft_datetimepicker").show()
        }else{
            pickCon.addClass("gray")
            finalDelay.addClass("disabled")
            $(".xdsoft_datetimepicker").hide()
        }

        read.on("change",function(){
            if(read.prop("checked")){
                pickCon.removeClass("gray")
                finalDelay.removeClass("disabled")
                cant.hide()
                $(".xdsoft_datetimepicker").show()

                $.datetimepicker.setLocale('zh-TW');
                $('#datetimepicker').datetimepicker({
                    timepicker:false,
                    format:'Y-m-d',
                    inline:true,
                    defaultSelect: false,
                    defaultDate: false,
                    minDate:'<?= $o_row['date'] ?>',
                    maxDate:'+1970/04/05',
                    yearStart:'2019',
                    yearEnd:'2030',
                    lang:'zh-TW',
                    todayButton:false,
                    scrollMonth:false,
                    scrollInput:false,
                });
            }else{
                pickCon.addClass("gray")
                finalDelay.addClass("disabled")
                $(".xdsoft_datetimepicker").hide()
            }
        })

        finalDelay.on("click",function(){
            var orderNum = $(this).attr('data-orderNum');
            var delayData = $("#datetimepicker").val()
            var postData = {
                'orderNum':orderNum,
                'delayData':delayData,
            }
            if($(this).hasClass("disabled")){
                cant.show()
            }else{
                $.post('php_api/order_delay_api.php',postData,function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        setTimeout(function(){
                            location.href = 'member-travelManage.php'
                        },1000)
                    }
                },"JSON")
            }
        })

        var datePick = $("#datetimepicker");
        datePick.change(function(){
            var afterDate = $("#datetimepicker").val()
            $(".after").text(afterDate)
        })
        

    </script>
</body>
</html>