<?php 
require __DIR__. '/php_api/__db_connect.php';
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 關於我們</title>
    <link rel="stylesheet" href="css/aboutUs.css">

<?php $aboutUs = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <section>
            <video src="video/about1.mp4" autoplay loop></video>
            <article>
                <div class="cover">
                    <h2 class="aboutTitle" id="storyTitle">品牌故事</h2>
                </div>
                <div class="aboutCon">
                    <div class="storyCon">
                        <div class="story">
                            <h3>我們是誰?</h3>
                            <p>我們是北投，「忽浴」取自「淴浴」一詞，意指盥洗、泡 澡。<br/>同時我們也「呼籲」大家來北投泡湯之外,也能透過不同方式感受北投之美。</p>
                        </div>
                        <div class="story">
                            <h3>我們在做什麼?</h3>
                            <p>在日治時期北投成為溫泉產業重地，台灣的歷史與日本的文化交織在一起，我們想透過最美麗的方式讓你認識這裡。<br/>我們替你換上浴衣，我們對你說故事，我們和你一起同樂，我們讓你愛上北投...</p>
                        </div>
                    </div>
                </div>
            </article>
        </section>
        <section>
            <video src="video/about2.mp4" autoplay loop></video>
            <article class="r-article">
                <div class="cover">
                    <h2 class="aboutTitle" id="contactTitle">聯絡我們</h2>
                </div>
                <div class="aboutCon contactCon">
                    <ul class="contact">
                        <li class="contactItem">
                            <h3>門市電話</h3>
                            <p><span><i class="fas fa-phone"></i></span>02-2493-3366</p>
                            <a href="tel:+886-2-24933366" class="hyperlink">撥打電話</a>
                        </li>
                        <li class="contactItem">
                            <h3>門市地址</h3>
                            <p><span><i class="fas fa-map-marker-alt"></i></span>台北市北投區七星街2號</p>
                            <a href="https://goo.gl/maps/zQgGEgD13TLBZPd17" target="_blank" class="hyperlink">開啟地圖</a>
                        </li>
                        <li class="contactItem">
                            <h3>營業時間</h3>
                            <p><span><i class="fas fa-clock"></i></span>週一至日 上午9:30~下午18:30</p>
                        </li>
                    </ul>
                </div>
            </article>
        </section>
        <section>
            <video src="video/about3.mp4" autoplay loop></video>
            <article>
                <div class="cover">
                    <h2 class="aboutTitle" id="mailboxTitle">客服信箱</h2>
                </div>
                <div class="aboutCon mailboxCon">
                    <form action="" name="mailbox" class="mailbox" method="post" onsubmit="return csMail()">
                        <div class="mailboxError">請輸入正確的信箱格式</div>
                        <label for="CSMail">您的信箱</label>
                        <input type="text" name="CSMail" id="CSMail" placeholder="請提供您的電子信箱"><br>
                        
                        <div class="mailboxError">請選擇一個主題</div>
                        <label for="CSTheme">反映主題</label>
                        <select name="CSTheme" id="CSTheme">
                            <option disabled selected hidden>請選擇主題</option>
                            <option value="會員相關">會員相關</option>
                            <option value="服務相關">服務相關</option>
                            <option value="預約相關">預約相關</option>
                            <option value="其他">其他</option>
                        </select><br>
                        <div class="mailboxError">請說明您的意見或問題</div>
                        <label for="">詳細內容</label> 
                        <textarea name="CSText" id="CSText" class="CSText" onkeyup="autoGrow(this)" rows="3" placeholder="請詳細說明您的意見或問題"></textarea><br>
                        <input type="submit" class="CSSubmit" value="送出">
                    </form>
                </div>
            </article>
        </section>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>
        $(window).on('load',function(){
            $("video").prop("autoplay",true);
        })
        

        function autoGrow(textarea){
            var adjustedHeight=textarea.clientHeight;
                adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
                if (adjustedHeight>textarea.clientHeight){
                    textarea.style.height=adjustedHeight+'px';
                };
            };
        
        var $CSMail = $('#CSMail');
        var $CSTheme = $('#CSTheme');
        var $CSText = $('#CSText');
        var fields = [$CSMail, $CSTheme, $CSText];
        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

        function csMail(){
            
            fields.forEach(function(val){
                val.prev().prev(".mailboxError").hide();
            });

            var isPass = true;

            if(! validateEmail($CSMail.val())){
                isPass = false;
                $CSMail.prev().prev(".mailboxError").show()
            }
            
            if(! $CSTheme.val()){
                isPass = false;
                $CSTheme.prev().prev(".mailboxError").show()
            }

            if($CSText.val().length==0){
                isPass = false;
                $CSText.prev().prev(".mailboxError").show()
            }

            if(isPass){
                var d = new Date();
                var csTime=d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes();
                var cs=$(".mailbox").serializeArray();

                var postData = {
                    'cs':cs,
                    'csTime':csTime
                }

                $.post('php_api/cs_write_api.php',postData,function(data){
                    if(data.success){
                        $(".successText").html(data.info);
                        $(".success").fadeIn().delay(1500).fadeOut();
                        // console.log(data.postData)
                        setTimeout(function(){
                            location.href = 'aboutUs.php'
                        },1500)
                    }
                },"JSON")
            }

            return false
        }

        var cover = $(".cover")
        var windowWidth = $(window).outerWidth();
        
        if(windowWidth>768){
            cover.on("click",function(){
                $(this).toggleClass("open");
            })
        }
        if(windowWidth<=768){
            $(".aboutCon").hide()
            cover.on("click",function(){
                var pageHeight = $("html").outerHeight();
                $(this).next(".aboutCon").slideToggle()
                if($(this).children().text()=="客服信箱"){
                    $("html").animate({scrollTop: pageHeight},500)
                }else if($(this).children().text()=="聯絡我們"){
                    $("html").animate({scrollTop: $(this).offset().top},500)
                }
            })
        }

        $(window).on('resize',function(){
            cover.off()
            windowWidth = $(window).outerWidth();
            if(windowWidth>768){
                $(".aboutCon").slideDown()
                cover.removeClass("open")
                cover.on("click",function(){
                    $(this).toggleClass("open");
                })
            }
            if(windowWidth<=768){
                $(".aboutCon").hide()
                cover.removeClass("open")
                cover.on("click",function(){
                    var pageHeight = $("html").outerHeight();
                    $(this).next(".aboutCon").slideToggle()
                    if($(this).children().text()=="客服信箱"){
                        $("html").animate({scrollTop: pageHeight},500)
                    }else if($(this).children().text()=="聯絡我們"){
                        $("html").animate({scrollTop: $(this).offset().top},500)
                    }
                })
            }
        })

    </script>
</body>
</html>