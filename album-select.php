<?php require __DIR__.'/php_api/__db_connect.php';?>

<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 相冊衣覽-著物展示</title>
    <link rel="stylesheet" href="css/album-select.css">

<?php $album = ""?>
<?php require __DIR__.'/__html_body.php'?>
    
    <div class="container">
        <header>
            <h1 class="title">相冊衣覽</h1>
        </header>
        <main>
            <div class="selectCon">
                <div class="ukata">
                    <a href="album-clothes.php">
                        <img src="images/album-select/album-select01.jpg" alt="">
                        <div class="con">
                            <div class="utextshap">
                                <div class="sq"></div>
                                <div class="utsq">
                                    <h2 class="utext">著物展示</h2>
                                    <p class="utext2">先來看看喜歡的浴衣花色吧，行程當天就可以馬上搖身一變喔！</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="guestPhoto">
                    <a href="album-guest.php">
                        <img src="images/group5.jpg" alt="">
                        <div class="con top">
                            <div class="utextshap">
                                <div class="sq"></div>
                                <div class="utsq">
                                    <h2 class="utext">客樣寫真</h2>
                                    <p class="utext2">瀏覽過往旅客的寫真實紀與他們的足跡，並期待你也來留下一筆喔！</p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </main>
    </div>

<?php require __DIR__.'/__html_js.php'?>

</body>
</html>