<?php
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
}
if($_SESSION['user']['sid']==1){
    header('Location: member-travelManage_master.php');
    exit;
}

$member_sid = $_SESSION['user']['sid'];
$o_sql = "SELECT * FROM `orders` WHERE 1 AND `belong`= $member_sid ORDER BY `order_status` ASC,`sid` DESC";
$o_stmt = $pdo->query($o_sql);
$rs = $o_stmt->fetchAll();

?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
                <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- -------------------------------------travelManage------------------------------------------ -->
        <section class="travelManage">
            <?php if(empty($rs)){echo '<p class="travelEmpty empty">您目前還沒有預約任何行程，快到<a href="product-select.php">「浴衣漫遊」</a>選一個適合你的浴衣體驗吧！</p>';} ?>
            <?php foreach($rs as $r): ?>
            <?php 
                $order_number = $r['order_number']; 
                $d_sql = "SELECT COUNT(1) FROM `orders_details` WHERE 1 AND `order_number`= $order_number";
                $totalRows = $pdo->query($d_sql)->fetch(PDO::FETCH_NUM)[0];
                $totalOther = $totalRows-1;
                
                $m_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1";
                $m_stmt = $pdo->query($m_sql);
                $m_row = $m_stmt->fetch();
                
                $ot_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1 , $totalOther";
                $ot_stmt = $pdo->query($ot_sql);
                $ot_row = $ot_stmt->fetchAll();

                $p_sql = "SELECT * FROM `pay_info` WHERE 1 AND `order_number`= $order_number";
                $p_stmt = $pdo->query($p_sql);
                $p_row = $p_stmt->fetch();

                $pay_date = $p_row['pay_date'];
                $o_date = $r['date'];
                $now_date = date("Y-m-d");
                $tda_date = date("Y-m-d",strtotime("-3 day",strtotime($o_date)));
                
                if($pay_date<$now_date && $r['pay_status']=="待付款"){
                    $r['pay_status']="逾期付款";
                    $r['order_status']="已取消";
                }else if($o_date<$now_date && $r['order_status']=="尚未完成"){
                    $r['order_status']="已完成";
                }
            ?>
            <div class="order <?= $r['order_status']=="已取消" ? "gray" : "" ?>">
                <div class="orderNum"><span class="num">訂單編號：<?= $r['order_number'] ?></span></div>
                <div class="orderInfo">
                    <div class="orderTitle">
                        
                        <h3><?php 
                            switch($r['type']){
                            case "walk":echo "浴衣體驗-散步方案";break;
                            case "photo":echo "浴衣體驗-攝影方案";break;
                            case "explore":echo "浴衣小旅行-探險路線";break;
                            case "history":echo "浴衣小旅行-軼聞路線";break; 
                        }?></h3>

                        <div class="orderAct" data-orderNum="<?= $r['order_number'] ?>">

                            <?php if($r['order_status']=="尚未完成" || $r['order_status']=="尚未完成(已延後)"): ?>
                            <button class="delay <?= $now_date>=$tda_date || $r['pay_status']=="待付款" || $r['delay']=="1" ? "disabled" : "" ?>">延後預約</button>
                            <?php endif; ?>

                            <?php if($r['pay_status']=="已付款" && $r['order_status']=="尚未完成" || $r['order_status']=="尚未完成(已延後)"): ?>
                            <button class="cancel">取消預約</button>
                            <?php endif; ?>

                            <?php if($r['order_status']=="已完成"): ?>
                            <button class="comment <?= $r['comment']==1 ? "disabled" : "" ?>">填寫評論</button>
                            <?php endif; ?>

                        </div>
                    </div>
                    <div class="orderDetail">
                        <div class="basicInfo">
                            <div class="term date"><h5>日期</h5><p><?= $r['date'] ?></p></div>
                            <div class="term time"><h5>時間</h5><p><?= $r['time']=="am" ? "上午 10:00" : "下午 14:00" ?></p></div>
                            <div class="term people"><h5>人數</h5><p><?= $r['people'] ?>人</p></div>
                        </div><hr>
                        <div class="basicInfo">
                            <div class="term plan"><h5>方案</h5><p><?php 
                            switch($r['plan']){
                                case  "inside":echo "棚內拍攝/每人NT$1,000";break;
                                case  "outside":echo "外景拍攝/每人NT$1,600";break;
                                case  "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                case  "two":echo "兩小時體驗/每人NT$500";break;
                                case  "four":echo "四小時體驗/每人NT$700";break;
                                case  "six":echo "六小時體驗/每人NT$850";break;
                                case  "twoFour":echo "二至四人/每人NT$2,000";break;
                                case  "fiveNine":echo "五至九人/每人NT$1,800";break;
                                case  "tenUp":echo "十人以上/每人NT$1,500";break;
                            }?></p></div>
                            <?php if($r['extra']>0):?>
                            <div class="term add"><h5>妝髮</h5><p><?= $r['extra'] ?>人/每人 NT$500</p></div>
                            <?php endif;?>
                        </div>
                        <hr>
                        <div class="paymentInfo">
                            <div class="term total"><h5>付款金額</h5><p><?= $r['total'] ?></p></div>
                            <div class="term travelStatus"><h5>付款方式</h5><p><?= $r['pay_method'] ?></p></div>
                            <div class="term paymentStatus"><h5>付款狀態</h5><p class="pStatus <?= $r['pay_status']=="已付款" ? "yes" : "no" ?>"><?= $r['pay_status'] ?></p></div>
                            <div class="term travelStatus"><h5>行程狀態</h5><p class="oStatus <?= $r['order_status']=="已完成" ? "already" : "notYet" ?>"><?= $r['order_status'] ?></p></div>
                        </div>
                    </div>
                </div>
                <?php if($r['pay_status']=="待付款"): ?>
                <div class="payInfo orderDetail">
                    <div class="basicInfo">
                        <div class="term"><h5>銀行代碼</h5><p>013國泰世華銀行</p></div>
                        <div class="term"><h5>轉帳帳號</h5><p><?= $p_row['atm_account'] ?></p></div>
                    </div>
                    <div class="basicInfo">
                        <div class="term"><h5>轉帳金額</h5><p><?= $r['total'] ?></p></div>
                        <div class="term"><h5>繳款期限</h5><p><?= $p_row['deadline'] ?></p></div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="travelerInfo">
                    <div class="travelerTitle">
                        <h3><span class="show"><i class="fas fa-plus-square"></i></span> 旅客資料</h3>

                        <?php if($r['order_status']=="尚未完成" || $r['order_status']=="尚未完成(已延後)"): ?>
                        <div class="orderAct"><button class="modify <?= $now_date>=$tda_date || $r['pay_status']=="待付款" ? "disabled" : "" ?>">修改資料</button></div>
                        <?php endif;?>

                    </div>
                    <div class="travelerContent">
                        <div class="travelerForm">
                            <h3 class="traveler">團員1(主要聯絡人)</h3>
                            <div class="travelerDetail">
                                <div class="term traveler-name"><h5>旅客姓名</h5><p><?= $m_row['name'] ?></p></div>
                                <div class="term traveler-id"><h5>身分證字號</h5><p><?= $m_row['identification'] ?></p></div>
                                <div class="term traveler-mobile"><h5>行動電話</h5><p><?= $m_row['mobile'] ?></p></div>
                                <div class="term traveler-email"><h5>電子信箱</h5><p><?= $m_row['email'] ?></p></div>
                            </div>
                            <hr>
                        </div>
                        <?php if($ot_stmt->rowCount()>0):?>
                        <?php $i=1 ?>
                        <?php foreach($ot_row as $row):?>
                        <?php $i+=1 ?>
                        <div class="travelerForm">
                            <h3 class="traveler">團員<?= $i ?></h3>
                            <div class="travelerDetail">
                                <div class="term traveler-name"><h5>旅客姓名</h5><p><?= $row['name']?></p></div>
                                <div class="term traveler-id"><h5>身分證字號</h5><p><?= $row['identification']?></p></div>
                            </div>
                            <hr>
                        </div>
                        <?php endforeach;?>
                        <?php endif;?>
                    </div>
                </div>
                <div class="orderFoot">                
                </div>
            </div>
            <?php endforeach;?>
        </section>

        <div class="commentWindows" style="display:none">
            <form class="comment-form" action="" name="walk-comment" method="post" onsubmit="return text()">
            <div class="commentClose"><i class="fas fa-times fa-fw"></i></div>
                <fieldset class="rating-group">
                    <label for="s1" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                    <input type="radio" name="star" id="s1" class="radio" value="1">

                    <label for="s2" class="rating-star"><i class="fas fa-star no-rate rating"></i></label> 
                    <input type="radio" name="star" id="s2" class="radio" value="2">
                
                    <label for="s3" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                    <input type="radio" name="star" id="s3" class="radio" value="3">
                    
                    <label for="s4" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                    <input type="radio" name="star" id="s4" class="radio" value="4">
                    
                    <label for="s5" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                    <input type="radio" name="star" id="s5" class="radio" value="5">
                    <span style="float:right " class="check check1">請選擇星等</span>
                </fieldset>
                <p class="check check2">請使用至少20個字說明</p>
                <textarea name="comment" id="comment-text" class="comment-text" placeholder="請使用至少20個字說說你的親身體驗" onkeyup="autoGrow(this);" rows="5"></textarea><br/>
                <div class="submit-group">
                    <button class="submit">送出評論</button>
                </div>
            </form>
        </div>

    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>


// -------------------------------------------------------------------------------------------------------
        var orderNum;

        $(".show").click(function(){
            $(this).children().toggleClass("fa-plus-square").toggleClass("fa-minus-square");
            $(this).closest(".travelerTitle").next(".travelerContent").slideToggle()
        })
// -------------------------------------------------延後預約------------------------------------------------------
        $("html").on("click",".delay",function(){
            var pStatus = $(this).closest(".order").find(".pStatus");
            var oStatus = $(this).closest(".order").find(".oStatus");
            var orderNum = $(this).parent().attr("data-orderNum");

            if($(this).hasClass("disabled") && oStatus.text()=="尚未完成(已延後)"){
                $(".errorText").html("您的預約已延後過<br>如欲再次延後請取消預約後再次預訂");
                $(".error").fadeIn().delay(1000).fadeOut();
            }else if($(this).hasClass("disabled") && pStatus.text()=="待付款"){
                $(".errorText").text("請先付款才能使用此功能");
                $(".error").fadeIn().delay(1000).fadeOut();
            }else if($(this).hasClass("disabled") && pStatus.text()=="已付款"){
                $(".errorText").html("行程出發三天前<br>才可延後預約");
                $(".error").fadeIn().delay(1000).fadeOut();
            }else{
                location.href = 'member-travelManage-delay.php?orderNum='+orderNum;
            }
        });
// -------------------------------------------------取消預約------------------------------------------------------
        $("html").on("click",".cancel",function(){
            var orderNum = $(this).parent().attr("data-orderNum");
            location.href = 'member-travelManage-cancel.php?orderNum='+orderNum;
        });
// -------------------------------------------------評論------------------------------------------------------
        $("html").on("click",".comment",function(){
            orderNum = $(this).parent().attr("data-orderNum");
            $.post('php_api/member_comment_api.php',{'orderNum':orderNum},function(data){
                if(data.code==000){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                }else if(data.code==111){
                    $(".commentWindows").fadeIn()
                }
            },"JSON")
        });
// -------------------------------------------------修改旅客資料------------------------------------------------------
        $("html").on("click",".modify",function(){
            var pStatus = $(this).closest(".order").find(".pStatus");
            var orderNum = $(this).closest(".order").find(".orderAct").attr("data-orderNum");

            if($(this).hasClass("disabled") && pStatus.text()=="待付款"){
                $(".errorText").text("請先付款才能使用此功能");
                $(".error").fadeIn().delay(1000).fadeOut();
            }else if($(this).hasClass("disabled") && pStatus.text()=="已付款"){
                $(".errorText").html("行程出發三天前<br>才可修改資料");
                $(".error").fadeIn().delay(1000).fadeOut();
            }else{
                location.href = 'member-travelManage-modify.php?orderNum='+orderNum;
            }
        });
// -------------------------------------------------視窗評論------------------------------------------------------
        var commentBtn = $(".comment-btn");
        function autoGrow(textarea){
        var adjustedHeight=textarea.clientHeight;
            adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
            if (adjustedHeight>textarea.clientHeight){
                textarea.style.height=adjustedHeight+'px';
            };
        };
        
        $(".commentClose").click(function(){
            $(".commentWindows").fadeOut()
        });

        $(".radio").change(function(){
                $(this).prevAll(".rating-star").children().removeClass("no-rate");
                $(this).nextAll(".rating-star").children().addClass("no-rate");
        });

        var star = $('.radio');
        var starNum = $('.radio').length
        var starInput = false
        var starCheck = $(".check1")
        var comment = $(".comment-text")
        var commentCheck = $(".check2")
        var commentFields = [starCheck, commentCheck];

        function text(){
            commentFields.forEach(function(val){
                val.hide();
            });

            var isPass = true;

            for(var i=0;i<starNum;i++){ 
                if(star[i].checked) 
                starInput=true; 
            }
            if(!starInput) {
                isPass = false;
                starCheck.show()
            }
            if(comment.val().length<20){
                isPass = false;
                commentCheck.show()
            }
            console.log(isPass)
            if(isPass){
                var commentData = {
                    "order_number": orderNum,
                    "star": $('.radio:checked').val(),
                    "text": comment.val()
                }

                $.post('php_api/comment_insert_api.php',commentData, function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        $(".commentWindows").fadeOut()
                        setTimeout(function(){
                            location.href = 'member-travelManage.php'
                        },1000)
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(800).fadeOut();
                        
                    }
                }, "json")
            }
            return false
        }

    </script>
</body>
</html>