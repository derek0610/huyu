<?php 
require __DIR__. '/php_api/__db_connect.php';

if(! isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
}

require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>

<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
            <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn select"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn select"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- -------------------------------------profile------------------------------------------ -->
        <section class="profile">
            <div class="orderNum"><span class="num">&nbsp</span></div>
            <div class="profileTitle">
                <h3>會員資料</h3>
                <div class="profileAct">
                    <button class="edit">編輯資料</button>
                    <button class="cancelEdit noShow">取消編輯</button>
                </div>
            </div>
            <form class="profileInfo" name="profileInfo" action="" method="post" onsubmit="return editInfo()">

                <label for="username">
                    <p><i class="fas fa-circle"></i> 會員姓名</p>
                </label>
                <small>請輸入真實姓名</small>
                <input type="text" name="username" id="username" value="<?= $_SESSION['user']['name'] ?>" readonly>


                <label for="account">
                    <p><i class="fas fa-circle"></i> 帳號(電子信箱)</p>
                </label>
                <input type="text" name="account" id="account" value="<?= $_SESSION['user']['email'] ?>" readonly>


                <label for="password">
                    <p><i class="fas fa-circle"></i> 密碼</p>
                </label>
                <small>密碼需至少6個英文字母或數字</small>
                <input type="password" name="password" id="password" value="<?= $_SESSION['user']['password'] ?>" placeholder="輸入原密碼或更新密碼" readonly>


                <label for="passwordAgain" class="noShow">
                    <p><i class="fas fa-circle"></i> 再次輸入密碼</p>
                </label>
                <small>兩次輸入密碼不相同</small>
                <input type="password" class="noShow" name="passwordAgain" id="passwordAgain" placeholder="再次輸入密碼" readonly>


                <label for="birthday">
                    <p><i class="fas fa-circle"></i> 生日</p>
                </label>
                <small>請輸入正確的日期格式</small>
                <input type="text" name="birthday" id="birthday" value="<?= $_SESSION['user']['birthday'] ?>" placeholder="1911-01-01" readonly>


                <label for="mobile">
                    <p><i class="fas fa-circle"></i> 行動電話(預約時自動填寫)</p>
                </label>
                <small>請輸入正確的電話號碼格式</small>
                <input type="text" name="mobile" id="mobile" value="<?= $_SESSION['user']['mobile'] ?>" placeholder="0900-000-000" readonly>

                <div class="submitCon noShow"><input type="submit" class="submit" value="確認修改"></div>
            </form>
        </section>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>
        var $username = $('#username');
        var $account = $('#account');
        var $password = $('#password');
        var $passwordAgain = $('#passwordAgain');
        var $birthday = $('#birthday');
        var $mobile = $('#mobile');
        var fields = [$username, $password, $passwordAgain, $birthday, $mobile];
        var mobileRegex = /^09\d{2}\-\d{3}\-\d{3}$/;
        var birthdayRegex = /^\d{4}-\d{2}-\d{2}$/

        function editInfo(){
            
            fields.forEach(function(val){
                val.prev("small").hide();
            });

            var isPass = true;

            if($username.val().length < 2) {
                isPass = false;
                $username.prev("small").show()
            }
            
            if($password.val().length < 6) {
                isPass = false;
                $password.prev("small").show()
            }

            if($passwordAgain.val()!=$password.val()) {
                isPass = false;
                $passwordAgain.prev("small").show()
            }

            if(!birthdayRegex.test($birthday.val())) {
                isPass = false;
                $birthday.prev("small").show()
            }

            if(!mobileRegex.test($mobile.val())) {
                isPass = false;
                $mobile.prev("small").show()
            }

            if(isPass){
                $.post('php_api/profile_edit_api.php', $(".profileInfo").serialize(), function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(500).fadeOut();
                        setTimeout(function(){location.reload()},1000)
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(1000).fadeOut();
                    }
                }, "json")
            }

            return false
        }

        var editItem = $(".profileInfo input").not("#account").not(".submit");
        var noShow = $(".noShow");
        noShow.hide();

        var originPassword = $("#password").val()
        $(".edit").click(function(){
            noShow.fadeIn()
            editItem.prop("readonly",false);
            editItem.css("border","1px solid #aaa")
            $("#password").val("")
            $(this).hide();
        })
        $(".cancelEdit").click(function(){
            noShow.hide()
            editItem.prop("readonly",true);
            editItem.css("border","none");
            $("#password").val(originPassword)
            $(".edit").show();
            fields.forEach(function(val){
                val.prev("small").hide();
            });
        })
    </script>
</body>
</html>