<?php
require __DIR__. '/php_api/__db_connect.php';

if((!isset($_SESSION['user'])) or ($_SESSION['user']['sid']!==1)){
    header('Location: index.php');
    exit;
}
    
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 客服信箱</title>
    <link rel="stylesheet" href="css/member.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<?php require __DIR__.'/__html_js.php'?>
<script src="js/vue.js"></script>
<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
            <h1 class="title">客服信箱</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog"></i> 後臺管理</a>
                <a href="member-travelManage_master2.php" class="memberBtn select"><i class="far fa-comment-dots" aria-hidden="true"></i> 客服信箱</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- -------------------------------------travelManage------------------------------------------ -->
        <section class="travelManage" id="ord_vue">
            <div class="order" v-for="ord in ords" v-bind:class="[ord.order_status=='已取消' ? 'gray' : '']" >
                <form class="orderInfoReport" name="orderInfo" action="" method="post" >
                    <div class="orderInfo">
                        <div class="orderTitle masterbgc">
                            <h3>{{ord.theme}}<span v-if="ord.answer==''">(尚未處理)</span></h3>
                            
                            <div class="orderAct" v-bind:data-sid="ord.sid">
                                <input type="button" class="payChange" value="回覆">
                                <input type="submit" class="payChangeYes" value="送出" style="display: none" >
                            </div>                                              
                        </div>
                        <div class="orderDetail">
                            <div class="basicInfo">
                                <div class="term name"><h5>Email</h5><p>{{ord.email}}</p></div>
                                <div class="term date"><h5>時間</h5><p>{{ord.time}}</p></div>
                            </div><hr>
                            <div class="basicInfo">    
                                <div class="text"><h5>內容</h5><p>{{ord.text}}</p></div>
                            </div><hr>
                            <div class="basicInfo"> 
                                <div class="div_answer">
                                    <h5>處理</h5>
                                    <p>
                                        <div class="ans">{{ord.answer}}</div>
                                        <textarea class="answer" cols="50" rows="10" name="欄位名稱" placeholder="{{ord.answer}}" style="display: none"></textarea>
                                    </p></div>
                            </div><hr>
                        </div>
                    </div>
                    <div class="payInfo orderDetail" v-if="ord.pay_status=='待付款'">
                        <div class="basicInfo">
                            <div class="term"><h5>銀行代碼</h5><p>013國泰世華銀行</p></div>
                            <div class="term"><h5>轉帳帳號</h5><p>{{ord.atm_account}}</p></div>
                        </div>
                        <div class="basicInfo">
                            <div class="term"><h5>轉帳金額</h5><p>{{ord.total}}</p></div>
                            <div class="term"><h5>繳款期限</h5><p>{{ord.deadline}}</p></div>
                        </div>
                    </div>
                    <div class="orderFoot masterbgc"></div>
                </form>
            </div>
        </section>
    </div>
<!-- -------------------------------------script-------------------------------------------->
    <script>
        var vm=new Vue({
            el: "#ord_vue",
            data: {
                ords: []
            },
            ready: function(){
                $.ajax({
                    url: "php_api/email_show_api.php",
                    type: "POST",
                    cache:false,
                    dataType: 'json',
                    success: function(res){
                        vm.ords=res;
                    }
                });
            },
        });
    </script>
    <script>

        $("html").on("click",".payChange",function(){
            var thisOrder=$(this).parentsUntil(".order")
            $(this).hide().next().show();
            thisOrder.find(".form-control").show().prev().hide();
            thisOrder.find(".answer").show();
            thisOrder.find(".ans").hide();
        });

        $("html").on("click",".payChangeYes",function(){
            var thisOrder=$(this).parentsUntil(".order")
            $(this).hide().prev().show();
            thisOrder.find(".form-control").hide().prev().show();

            var sid = $(this).parent().attr("data-sid");
            var answer=thisOrder.find(".answer").val();

            $.post('php_api/email_answer_api.php', "sid="+sid+"&answer="+answer, function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(500).fadeOut();
                        setTimeout(function(){location.reload()},1000)
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(1000).fadeOut();
                    }
                }, "json")
            
        });

        // function editInfo(){
            
        // }

    </script>
</body>
</html>