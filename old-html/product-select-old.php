<?php
require __DIR__. '/php_api/__db_connect.php';
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 浴衣漫遊-散步方案</title>
    <link rel="stylesheet" href="css/product-select.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
            <h1 class="title">浴衣漫遊</h1>
            <p class="intro">到北投玩別再只是泡溫泉了!「忽浴」提供多樣豐富的體驗方式讓你身穿日式浴衣悠遊北投，趣味闖關、深度之旅、輕鬆自由行，挑個適合你的旅遊方案吧!</p>
        </header>
<!-- ------------------------------------------------------------------------------- -->
        <section>
            <article>
                <div class="sort">
                    <div class="sortTitle"><div class="dots"><i class="fas fa-circle"></i></div><h2>浴衣體驗</h2></div>
                    <div class="typeCon">
                        <a href="product-exp-walk.php" class="type">
                            <img src="images/female2.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">散步方案</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要輕鬆一點，「散步方案」可以讓你自由地走在北投的古道上。</p>
                            </div>
                        </a>
                        <a href="product-exp-photo.php" class="type">
                            <img src="images/linda8.png" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">攝影方案</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要紀錄美好的回憶，「寫真方案」能夠幫你封存旅行中的快樂。</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="sort">
                    <div class="sortTitle"><div class="dots"><i class="fas fa-circle"></i></div><h2>浴衣小旅行</h2></div>
                    <div class="typeCon">
                        <a href="product-trip-history.php" class="type">
                            <img src="images/Beitou3.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">軼聞路線</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要走進歷史，文藝風情的「軼聞路線」將帶你見證北投的變遷。</p>
                            </div>
                        </a>
                        <a href="product-trip-explore.php" class="type">
                            <img src="images/game2.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">探索路線</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要來場冒險遊戲，「探索路線」中有許多秘密等著你發掘。</p>
                            </div>
                        </a>
                    </div>
                </div>
            </article>
        </section>
    </div>

<?php require __DIR__.'/__html_js.php'?>

</body>
</html>