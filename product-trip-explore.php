<?php
require __DIR__. '/php_api/__db_connect.php';

?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 浴衣漫遊-探索路線</title>
    <link rel="stylesheet" type="text/css" href="slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">
    <link rel="stylesheet" href="css/product.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

        <section class="photoSlider">
            <div class="wave"></div> 
            <div class="photo-group">
                <img src="images/game2.jpg" alt="">
                <img src="images/game1.jpg" alt="">
                <img src="images/explain1.jpg" alt="">
                <img src="images/explain3.jpg" alt="">
                <img src="images/Hotspring-museum4.jpg" alt="">
                <img src="images/Long-Nai-Tang.jpg" alt="">
                <img src="images/Thermal-Valley4.jpg" alt="">
            </div>
        </section>
        <main>
            <header>
                <h1 class="title">浴衣小旅行-探險路線</h1>
                <h3>充滿祕密的磺泉秘境，等你來一探究竟。</h3>
                <p class="intro">你知道北投的命名由來與女巫有關嗎?你知道北投溫泉是甚麼湯種嗎?你知道北投的溫泉眷村裡藏著甚麼不可告人的祕密嗎?跟著「忽浴」一起探險，發掘線索，找出藏在北投的珍貴寶藏吧!</p>
                <div class="act">
                    <ul class="info">
                        <li><img src="images/element/icon_clock.svg" alt="">2.5小時</li>
                        <li><img src="images/element/icon_avatar.svg" alt="">2人成行</li>
                        <li><img src="images/element/icon_globe.svg" alt="">中文導覽</li>
                    </ul>
                    <button class="reserve">馬上預約</button>
                    <ul class="share">
                        <li><img src="images/element/icon_fb.svg" alt=""></li>
                        <li><img src="images/element/icon_twitter.svg" alt=""></li>
                        <li><img src="images/element/icon_line.svg" alt=""></li>
                    </ul>
                </div>
            </header>
            <ul class="product-nav">
                <li class="special p-active">活動特色</li>
                <li class="detail">詳細內容</li>
                <li class="comment">旅客評論</li>
            </ul>
            <article class="product-content">
                <ul class="content special show">
                    <!-- <video src="video/no-logo.mp4" controls autoplay muted></video> -->
                    <li class="group">
                        <h3 class="special-title">遊戲x體驗x學習</h3>
                        <p class="special-content">「忽浴」團隊有專業的環境教育人員、輔導活動教師、更有實力堅強的北投文化顧問，我們結合旅遊與教育，設計了一連串互動解謎遊戲，讓大朋友與小朋友都能在遊戲的過程中能夠學習到北投當地的地質環境、文化變遷、溫泉景觀等等有趣的小知識，並且在玩樂中欣賞北投的美景，享受特別的旅程。</p>
                    </li>
                    <li class="group">
                        <h3 class="special-title">日式浴衣體驗</h3>
                        <p class="special-content">「忽浴」提供完善的日式浴衣換裝體驗，讓漫步在北投溫泉街的您，不用遠赴日本也能穿著正統日式浴衣，用不同方式感受北投的人文風情，我們提供的和服和浴衣款式齊全多樣，每個人都能挑到自己喜歡的花樣，最棒的是還包含免費的配件和髮飾。你也可以先到 <a href="album-clothes.php"><i class="fas fa-hand-point-right"></i>「相冊衣覽-著物展示」</a>找找喜歡的浴衣款式喔!</p>
                    </li>
                    <li class="group">
                        <h3 class="special-title">專業導覽人員</h3>
                        <p class="special-content">不想看死板板的維基百科嗎?想聽聽「巷子內」才知道的北投小故事嗎？「忽浴」有最專業、活潑的導覽員，無論是自然景觀、歷史人文、建築設計，甚至當地傳說都難不倒他們喔。</p>
                    </li>
                    <li class="group">
                        <h3 class="special-title">精美伴手禮</h3>
                        <p class="special-content">享受完一場文化洗禮怎麼能不帶點特別的回家呢，現在只要在旅程的最後幫我們在網站上留下你珍貴的意見，我們就送給每一位團員北投當地的精美小禮物喔！</p>
                    </li>
                </ul>
                <ul class="content detail ">
                    <li class="group">
                        <h3 class="detail-title">費用包含</h3>
                        <ul class="detail-content">
                            <li>全套浴衣、配件搭配及換裝（現場挑選）。</li>
                            <li>2.5小時北投觀光導覽（含著裝時間）。</li>
                            <li>各景點門票/服務費。</li>
                            <li>旅遊平安險、公共意外險。</li>
                            <li>現場線上評論，每位團員均可獲精美紀念品。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">流程概述</h3>
                        <ul class="detail-content">
                            <li>提前15分鐘到店。</li>
                            <li>選擇和服樣式（10分鐘）。</li>
                            <li>穿戴（15分鐘）。</li>
                            <li>跟著導覽員出發，享受旅程（120分鐘）。</li>
                            <li>返回門市，進行換裝。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">行程介紹</h3>
                        <div class="detail-content">
                            <h5>臺北市立圖書館北投分館</h5>
                            <p>臺北市立圖書館北投分館是臺灣首座綠建築圖書館，座落於林木茂密、生態環境豐富的北投公園內，與溫泉博物館比鄰。屋頂為輕質生態屋頂，設有太陽能光電板發電，可發電16千瓦電力，並採大量陽台深遮陽及垂直木格柵，降低熱輻射進入室內，降低耗能達到節能效果。綠化屋頂及斜坡草坡設計可涵養水分自然排水至雨水回收槽，再利用回收水澆灌植栽及沖水馬桶，達到綠化與減少水資源浪費。</p>
                            <hr>
                            <h5>地熱谷</h5>
                            <p>地熱谷是一處硫氣及溫泉的出口，泉水屬酸性硫磺泉，水溫高達攝氏90度，，位在台灣北投中山路的陽明山山谷窪地，當地人又稱「磺水頭」或「鬼湖」 ，因長年蒸氣瀰漫、熱氣騰騰，使人聯想成恐怖的地獄。早年亦曾有遊客失足跌入高溫泉水中，被活活燙死，因而另有別名「地獄谷」。</p>
                            <hr>
                            <h5>北投市場</h5>
                            <p>「北投市場」是台北市歷史最悠久的公有傳統市場之一，想了解在地生活，挖掘在地人從小吃到大的隱藏美食，往傳統市場裡走去就對了！北投市場內有很多老字號的傳統小吃攤，更是許多人從小吃到大的成長記憶，美味傳奇的程度，不只在地人喜歡，更讓很多民眾慕名拜訪只為滿足那挑剔的口腹之慾！</p>
                            <hr>
                            <h5>天狗庵史蹟公園</h5>
                            <p>天狗庵成立於西元1896年，為台灣第一間民營溫泉旅館「天狗庵」藏身於台北市，見證北投溫泉發展史，但隨著日人離開而荒廢，北市府決定原址（現溫泉路73巷）打造「天狗庵史蹟公園」，營造最天然的日式庭園景觀。</p>
                            <hr>
                            <h5>中心新村</h5>
                            <p>中心新村原為陸軍衛戍醫院北投轉地療養所，戰後作為三軍總醫院北投分院的眷村，眷戶多為此醫院之醫生、護士、軍官士官、及來此療養之軍人與其眷屬。是全臺唯一溫泉軍醫眷村，也是臺北市唯一全區保存的眷村，為國防部通過全臺13處眷村保存區之一，屬於北投生活環境博物園區的重要歷史現場之一。</p>
                        </div>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">溫馨提醒</h3>
                        <ul class="detail-content">
                            <li>夏季建議穿薄內衣，背心。</li>
                            <li>冬季建議穿防寒內衣或輕薄夾克（低領）、緊身褲或保暖褲（襪套除外）來禦寒。</li>
                            <li>注意不要穿有領的襯衫，容易乾擾和服的展示。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">注意事項</h3>
                        <ul class="detail-content">
                            <li>浴衣款式依現場提供為主，無提供事先預訂服務。</li>
                            <li>當天請於行程出發前15分鐘至「忽浴」櫃檯報到，如未依規定時間到達，「忽浴」有權利調整行程內容。</li>
                            <li>行程當天請攜帶證件，並抵押至櫃台，行程結束將全數歸還。</li>
                            <li>活動全程均有保險，請提供完整旅客資訊以利辦理。</li>
                            <li>浴衣及相關配件如有無法恢復之污漬、破損、遺失，將以現金收取賠償費用。</li>
                            <li>0-2歲兒童如有攜帶證件，可免費參與本活動。</li>
                            <li>兒童浴衣，僅適合身高100公分至140公分之兒童穿著。</li>
                            <li>孕婦無法穿著浴衣，若參與行程將不另外退費。</li>
                            <li>我們提供行李寄放服務，貴重物品請隨身攜帶，「忽浴」不負保管責任。</li>
                        </ul>
                    </li>
                    <li class="group">
                        <h3 class="detail-title">取消與延後</h3>
                        <ul class="detail-content">
                            <li>出發前41天以前取消，將收取5%之費用。</li>
                            <li>出發前31至40天取消，將收取10%之費用。</li>
                            <li>出發前21至30天取消，將收取20%之費用。</li>
                            <li>出發前2至20天取消，將收取30%之費用。</li>
                            <li>出發前1天取消，將收取50%之費用。</li>
                            <li>出發當天取消、無故未到者，恕不退費。</li>
                            <li>72小時(三日)內將無法更改、延後預約內容及旅客資訊</li>
                            <li>每筆預約僅提供延後服務一次，請謹慎使用。</li>
                        </ul>
                    </li>
                </ul>
                <ul class="content comment ">
                    <button class="comment-btn">撰寫評論</button>
                    <form class="comment-form" action="" name="walk-comment" method="POST" onsubmit="return text()">
                        <fieldset class="rating-group">
                            <label for="s1" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s1" class="radio" value="1">

                            <label for="s2" class="rating-star"><i class="fas fa-star no-rate rating"></i></label> 
                            <input type="radio" name="star" id="s2" class="radio" value="2">
                        
                            <label for="s3" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s3" class="radio" value="3">
                          
                            <label for="s4" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s4" class="radio" value="4">
                          
                            <label for="s5" class="rating-star"><i class="fas fa-star no-rate rating"></i></label>
                            <input type="radio" name="star" id="s5" class="radio" value="5">
                            <span style="float:right " class="check check1">請選擇星等</span>
                        </fieldset>
                        <p class="check check2">請使用至少20個字說明</p>
                        <textarea name="comment" id="comment-text" class="comment-text" placeholder="請使用至少20個字說說你的親身體驗" onkeyup="autoGrow(this);" rows="3"></textarea><br/>
                        <div class="submit-group">
                            <button class="submit">送出評論</button>
                        </div>
                    </form>
                    <div class="commentCon">
                
                    </div>
                    <div class="pageCon">

                    </div>
                </ul>
            </article>
        </main>
        <div class="reserve-box">
            <div class="close"><div class="close-symbol"><i class="fas fa-times"></i></div></div>
            <form action="order_enter.php" class="reserve-form" name="explore-reserve" method="post" onsubmit="return pay()">
                <input type="hidden" name="type" value="explore">
                <input id="datetimepicker" type="text" name="date" value="<?= date("Y-m-d",strtotime('+4 day')) ?>">
                <div class="time-select">
                    <label for="time" class="time-label">時間</label>
                    <select name="time" id="time">
                        <option value="am">上午 10:00</option>
                        <option value="pm">下午 15:00</option>
                    </select>
                </div>
                <fieldset class="plan-select">
                    <legend class="plan-select-title">選擇方案</legend>
                        <input type="radio" id="twoFour" name="plan" class="plan-radio" value="twoFour" data-price="2000" checked>
                        <label for="twoFour" class="plan"><h3>二至四人</h3><h3 class="price"><span>每人</span>NT$2,000</h3></label>

                        <input type="radio" id="fiveNine" name="plan" class="plan-radio" value="fiveNine" data-price="1800">
                        <label for="fiveNine" class="plan"><h3>五至九人</h3><h3 class="price"><span>每人</span>NT$1,800</h3></label>

                        <input type="radio" id="tenUp" name="plan" class="plan-radio" value="tenUp" data-price="1500">
                        <label for="tenUp" class="plan"><h3>十人以上</h3><h3 class="price"><span>每人</span>NT$1,500</h3></label>
                </fieldset>
                <div class="people-select">
                    <label for="people" class="people-label">人數</label>
                    <div class="less">－</div>
                    <input type="text" id="people" name="people" value="2" readonly>
                    <div class="add">＋</div>
                </div>
                <input type="hidden" id="makeup" name="makeup" value="0" data-price="500" readonly>
                <div class="final">
                    <input type="text" name="total" id="total" value="NT$4,000" readonly>
                    <input type="submit" class="pay" value="預約結帳">
                </div>
            </form>
        </div>

<?php require __DIR__.'/__html_js.php'?>

    <script type="text/javascript" src="slick/slick.min.js"></script>
    <script src="js/jquery.datetimepicker.full.min.js"></script>
    <script src="js/lodash.js"></script>
    <script src="js/product-trip.js"></script>
    <script>

        var params = {
            'page': 1,
            'type': "explore"
        };

        <?php if(isset($_SESSION['tpOrder']) && $_SESSION['tpOrder']['type']=="explore"):?>
            $('#datetimepicker').val("<?= $_SESSION['tpOrder']['date'] ?>");
            $('#datetimepicker').datetimepicker({
                value: "<?= $_SESSION['tpOrder']['date'] ?>",
            });
            $('#time option[value="<?= $_SESSION['tpOrder']['time'] ?>"]').prop("selected",true);
            $('.plan-radio[value="<?= $_SESSION['tpOrder']['plan'] ?>"]').prop("checked",true);
            $('#people').val(<?= $_SESSION['tpOrder']['people'] ?>);
            $('#makeup').val(<?= $_SESSION['tpOrder']['makeup'] ?>);
            $('#total').val("<?= $_SESSION['tpOrder']['total'] ?>");
        <?php endif;?>
    
        function pay() {
            $.post('php_api/tp_order_api.php',$('.reserve-form').serialize(),function(data){
                console.log(data)

                if(data.code==444){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                }
                if(data.code==555){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                    $(".bgBlur").fadeIn();
                }
                if(data.code==1){
                    location.href = 'order-enter.php'
                }       
            },"json")
            return false;
        }
        // -------------------------------------------comment----------------------------------------
        var order_number
        commentBtn.click(function(){
            $.post('php_api/comment_api.php',{type:"explore"},function(data){
                if(data.code==444){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                    $(".bgBlur").fadeIn();
                }else if(data.code==000){
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(1000).fadeOut();
                }else if(data.code==111){
                    $(".comment-form").slideToggle();
                    order_number = data.orderNum
                }
            },"JSON")
        });

        var star = $('.radio');
        var starNum = $('.radio').length
        var starInput = false
        var starCheck = $(".check1")
        var comment = $(".comment-text")
        var commentCheck = $(".check2")
        var commentFields = [starCheck, commentCheck];

        function text(){
            commentFields.forEach(function(val){
                val.hide();
            });

            var isPass = true;

            for(var i=0;i<starNum;i++){ 
                if(star[i].checked) 
                starInput=true; 
            }
            if(!starInput) {
                isPass = false;
                starCheck.show()
            }
            if(comment.val().length<20){
                isPass = false;
                commentCheck.show()
            }
            console.log(isPass)
            if(isPass){
                var commentData = {
                    "order_number": order_number,
                    "star": $('.radio:checked').val(),
                    "text": comment.val()
                }

                $.post('php_api/comment_insert_api.php',commentData, function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        $(".comment-form").slideToggle();
                        reload({})
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(800).fadeOut();
                    }
                }, "json")
            }
            return false
        }

        var commentCon = $(".commentCon")
        var comment_str=`
            <li class="group">
                <div class="comment-title">
                    <span class="user"><%= name %></span>
                    <span class="star">
                        <%= starHtml %>
                    </span>
                    <span class="date"><%= time %></span> 
                </div>
                <p class="comment-content"><%= text %></p>
            </li>`;
        var comment_template = _.template(comment_str);

        var pageCon = $(".pageCon")
        var pageCon_str=`<a class="commentPage <%= page==i ? 'active' : '' %>" href="javascript:reload({page:<%= i %>})"><%= i %></a>`
        var pageCon_template = _.template(pageCon_str);

        function reload(obj){
            if(obj.page!==undefined){
                params.page = obj.page;
            }
            
            $.post('php_api/comment_show_api.php',params,function(data){
                var page = data.page;
                var totalPages = data.totalPages;

                pageCon.html("");
                for(var i=1;i<=totalPages;i++){
                    pageCon.append(pageCon_template({i:i, page:page}))
                }

                commentCon.html("");
                for(var i=0; i<data.rows.length; i++){
                    var name = data.rows[i].name
                    var time = data.rows[i].time
                    var text = data.rows[i].text
                    var star = data.rows[i].star
                    var starHtml = ''
                    for(let i=0;i<star;i++){
                        starHtml += '<i class="fas fa-star"></i>'
                    }
                    for(let i=0;i<5-star;i++){
                        starHtml += '<i class="fas fa-star no-rate"></i>'
                    }
                    var comment_array={
                        "name":name,
                        "time":time,
                        "text":text,
                        "starHtml":starHtml,
                    }
                    commentCon.append(comment_template(comment_array));
                }
            },"JSON")
        }

        reload({})
    </script>
</body>
</html>