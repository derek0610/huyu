<?php
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
}
if(!isset($_GET['orderNum'])){
    header('Location: member-travelManage.php');
    exit;
}

$member_sid = $_SESSION['user']['sid'];
$order_number = $_GET['orderNum'];

$d_sql = "SELECT COUNT(1) FROM `orders_details` WHERE 1 AND `order_number`= $order_number";
$totalRows = $pdo->query($d_sql)->fetch(PDO::FETCH_NUM)[0];
$totalOther = $totalRows-1;

$o_sql = "SELECT * FROM `orders` WHERE 1 AND `order_number`=$order_number AND `belong`=$member_sid";
$o_stmt = $pdo->query($o_sql);
$o_row = $o_stmt->fetch();

$m_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1";
$m_stmt = $pdo->query($m_sql);
$m_row = $m_stmt->fetch();

$ot_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1 , $totalOther ";
$ot_stmt = $pdo->query($ot_sql);
$ot_row = $ot_stmt->fetchAll();

?>
<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member-modify.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.min.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<!-- --------------------------------------header--------------------------------------------- -->
    <div class="container">
        <header id="app3" >
                <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- ---------------------------------travelManage-delay-------------------------------------- -->
        <main>
            <section class="travelManage">
                <div class="orderNum"><h2>修改旅客資料</h2><span class="num">訂單編號：<?= $o_row['order_number'] ?></span></div>
                    <div class="orderInfo">
                        <div class="orderTitle">
                            <h3><?php 
                                switch($o_row['type']){
                                case "walk":echo "浴衣體驗-散步方案";break;
                                case "photo":echo "浴衣體驗-攝影方案";break;
                                case "explore":echo "浴衣小旅行-探險路線";break;
                                case "history":echo "浴衣小旅行-軼聞路線";break; 
                            }?></h3>
                        </div>
                        <div class="orderDetail">
                            <div class="basicInfo">
                                <div class="term date"><h5>日期</h5><p><?= $o_row['date'] ?></p></div>
                                <div class="term time"><h5>時間</h5><p><?= $o_row['time']=="am" ? "上午 10:00" : "下午 14:00" ?></p></div>
                                <div class="term people"><h5>人數</h5><p><?= $o_row['people'] ?>人</p></div>
                            </div>
                            <hr>
                            <div class="basicInfo">
                            <div class="term plan"><h5>方案</h5><p><?php 
                                switch($o_row['plan']){
                                    case  "inside":echo "棚內拍攝/每人NT$1,000";break;
                                    case  "outside":echo "外景拍攝/每人NT$1,600";break;
                                    case  "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                    case  "two":echo "兩小時體驗/每人NT$500";break;
                                    case  "four":echo "四小時體驗/每人NT$700";break;
                                    case  "six":echo "六小時體驗/每人NT$850";break;
                                    case  "twoFour":echo "二至四人/每人NT$2,000";break;
                                    case  "fiveNine":echo "五至九人/每人NT$1,800";break;
                                    case  "tenUp":echo "十人以上/每人NT$1,500";break;
                                }?></p></div>
                                <?php if($o_row['extra']>0):?>
                                <div class="term add"><h5>妝髮</h5><p><?= $o_row['extra'] ?>人/每人 NT$500</p></div>
                                <?php endif;?>
                            </div>
                            <hr>
                            <div class="basicInfo">
                                <div class="term total"><h5>付款金額</h5><p><?= $o_row['total'] ?></p></div>
                                <div class="term total"><h5>付款方式</h5><p><?= $o_row['pay_method'] ?></p></div>
                                <div class="term paymentStatus"><h5>付款狀態</h5><p class="yes"><?= $o_row['pay_status'] ?></p></div>
                                <div class="term travelStatus"><h5>行程狀態</h5><p class="notYet"><?= $o_row['order_status'] ?></p></div>
                            </div>
                        </div>
                        <div class="delayTitle">
                            <h3>旅客資料</h3>
                        </div>
                        <div class="travelerContent">
                            <form action="" method="post" name="main" class="orderForm mainStaff">
                                <div class="travelerForm">
                                    <h3 class="traveler">團員1(主要聯絡人)</h3>
                                    <div class="travelerDetail">
                                        <div class="term">
                                            <h5>旅客姓名</h5>
                                            <small class="wrong">請輸入真實姓名</small>
                                            <input type="text" name="traveler-name" value="<?= $m_row['name'] ?>" placeholder="請輸入真實姓名" >
                                        </div>
                                        <div class="term">
                                            <h5>身分證字號</h5>
                                            <small class="wrong">請輸入正確的身分證字號</small>
                                            <input type="text" name="traveler-id" value="<?= $m_row['identification'] ?>" placeholder="A120000000" >
                                        </div>
                                        <div class="term">
                                            <h5>行動電話</h5>
                                            <small class="wrong">請輸入正確的電話號碼格式</small>
                                            <input type="text" name="traveler-mobile" value="<?= $m_row['mobile'] ?>" placeholder="0900-000-000" >
                                        </div>
                                        <div class="term traveler-email">
                                            <h5>電子信箱</h5>
                                            <small class="wrong">請輸入正確的電子信箱</small>
                                            <input type="text" name="traveler-email" value="<?= $m_row['email'] ?>" placeholder="輸入正確的電子信箱" >
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <hr>
                            <?php if($ot_stmt->rowCount()>0):?>
                            <?php $i=1 ?>
                            <?php foreach($ot_row as $row):?>
                            <?php $i+=1 ?>
                            <form action="" method="post" name="other" class="staff">
                                <h3 class="traveler">團員<?= $i ?></h3>
                                <div class="travelerDetail">
                                    <div class="term traveler-name">
                                        <h5>旅客姓名</h5>
                                        <small class="wrong">請輸入真實姓名</small>
                                        <input type="text" name="traveler-name" class="traveler_name" value="<?= $row['name']?>" placeholder="請輸入真實姓名" >
                                    </div>
                                    <div class="term traveler-id">
                                        <h5>身分證字號</h5>
                                        <small class="wrong">請輸入正確的身分證字號</small>
                                        <input type="text" name="traveler-id" class="traveler_id" value="<?= $row['identification']?>" placeholder="A120000000" >
                                        <input type="hidden" name="traveler-mobile" class="traveler_mobile" value>
                                        <input type="hidden" name="traveler-email" class="traveler_email" value>
                                    </div>
                                </div>
                                <hr>
                            </form>
                            <?php endforeach;?>
                            <?php endif;?>
                            <div class="final">
                                <button type="button" class="finalModify" data-orderNum="<?= $o_row['order_number'] ?>">確認送出</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>

<?php require __DIR__.'/__html_js.php'?>


    <script>
        $(".wrong").hide()

        $("html").on("click",".finalModify",function(){
            var $name = $('input[name="traveler-name"]')
            var $id = $('input[name="traveler-id"]')
            var $mobile = $('input[name="traveler-mobile"]')
            var $email = $('input[name="traveler-email"]')
            var fields = [$name, $id, $mobile, $email];
            var mobileRegex = /^09\d{2}\-\d{3}\-\d{3}$/;
            var idRegex = /^[A-Z]\d{9}$/;

            fields.forEach(function(val){
                val.prev(".wrong").hide();
            });
            
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            var isPass = true;

            $name.each(function(){
                var nameVal = $(this).val()
                if(nameVal.length<2){
                    isPass = false;
                    $(this).prev(".wrong").show();
                }
            })

            $id.each(function(){
                var idVal = $(this).val()
                if(!idRegex.test(idVal)){
                    isPass = false;
                    $(this).prev(".wrong").show();
                }
            })

            if(!mobileRegex.test($mobile.val())){
                isPass = false;
                $mobile.prev(".wrong").show()
            }
            
            if(!validateEmail($email.val())){
                isPass = false;
                $email.prev(".wrong").show()
            }

            var d = new Date();
            var modifyTime=d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes();
            var orderNum = $(".finalModify").attr('data-orderNum');
            var travelerNew=$(".mainStaff").serializeArray();

            var formData = {}
            var staffNum = $(".staff").length

            formData[0]=travelerNew

            for(var i=0;i<staffNum;i++){
                    var otherData = $(".staff").eq(i).serializeArray()
                    formData[i+1] = otherData
            }
            console.log(formData)


            var postData = {
                'orderNum':orderNum,
                'modifyTime':modifyTime,
                'travelerNew':formData
            }
            if(!isPass){
                $("html").animate({scrollTop:0},500);
            }else{
                $.post('php_api/order_modify_api.php',postData,function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        // console.log(data.postData)
                        setTimeout(function(){
                            location.href = 'member-travelManage.php'
                        },1000)
                    }
                },"JSON")
            }
        })
        

    </script>
</body>
</html>