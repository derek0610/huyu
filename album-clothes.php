<?php require __DIR__.'/php_api/__db_connect.php';?>

<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 相冊衣覽-著物展示</title>
    <link rel="stylesheet" href="css/album-clothes.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

<?php $album = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="patternSelect">
        <div class="pattern"><img src="images/pattern/red.jpg" alt=""></div>
        <div class="pattern"><img src="images/pattern/yellow.jpg" alt=""></div>
        <div class="pattern"><img src="images/pattern/green.jpg" alt=""></div>
        <div class="pattern"><img src="images/pattern/blue.jpg" alt=""></div>
        <div class="pattern"><img src="images/pattern/purple.jpg" alt=""></div>
        <div class="pattern"><img src="images/pattern/black.jpg" alt=""></div>
    </div>

    <div class="container">
        <header>
            <h1 class="title">相冊衣覽-著物展示</h1>
            <p class="intro">你可以在這裡尋找喜歡的浴衣款式，並把它放進你的收藏清單中，出發當天也可以出示給門市人員看看，讓我們幫你快速找到你喜歡的樣式喔。</p>
        </header>
        <form action="" method="post" name="selectorForm" class="selectorForm" onsubmit="return goSelect()">
            <div class="size">
                <h3 class="selectorName">尺寸</h3>
                <div class="selectorGroup">
                    <input type="checkbox" name="size[]" id="z-all" value="" class="z-selector" checked><label for="z-all">全</label>
                    <input type="checkbox" name="size[]" id="w" value="w" class="z-selector" ><label for="w">女</label>
                    <input type="checkbox" name="size[]" id="m" value="m" class="z-selector"><label for="m">男</label>
                    <input type="checkbox" name="size[]" id="k" value="k" class="z-selector"><label for="k">童</label>
                </div>
            </div>
            <div class="color">
                <h3 class="selectorName">顏色</h3>
                <div class="selectorGroup">
                    <input type="checkbox" name="color[]" id="c-all" value="" class="c-selector" checked><label for="c-all">全</label>
                    <input type="checkbox" name="color[]" id="r" value="r" class="c-selector" ><label for="r">赤</label>
                    <input type="checkbox" name="color[]" id="y" value="y" class="c-selector"><label for="y">茶</label>
                    <input type="checkbox" name="color[]" id="g" value="g" class="c-selector"><label for="g">綠</label>
                    <input type="checkbox" name="color[]" id="b" value="b" class="c-selector"><label for="b">青</label>
                    <input type="checkbox" name="color[]" id="p" value="p" class="c-selector"><label for="p">藤</label>
                    <input type="checkbox" name="color[]" id="bk" value="bk" class="c-selector"><label for="bk">墨</label>
                </div>
            </div>
            <input class="pageInput" type="hidden" name="page" value="1">
        </form>
        <div class="display">
            <div class="clothes">
            </div>
            <div class="pageCon">
            </div>
        </div>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script src="js/lodash.js"></script>
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <script>
        $(".z-selector").not("#z-all").change(function(){
            $("#z-all").prop("checked",false)
        })
        $("#z-all").change(function(){
            $(this).siblings().prop("checked",false)
        })

        $(".c-selector").not("#c-all").change(function(){
            $("#c-all").prop("checked",false)
        })
        $("#c-all").change(function(){
            $(this).siblings().prop("checked",false)
        })
// <!--------------------------------------Lodash---------------------------------------------------->

    var clothes_container = $('.clothes');
    var clothes_str=`
        <div class="clothesItem <%= size %> <%= color %>">
            <div class="imgCon">
                <a class="lightBox" cdata-fancybox="gallery" href="images/clothes/<%= big_photo %>">
                <img src="images/clothes/<%= small_photo %>" alt="">
                </a>
            </div>
            <div class="clothesFoot">
                <div class="tag">
                    <div class="sizeTag" style="background:<%=size_bgc %>"><%= size_text %></div>
                    <div class="colorTag" style="background:<%= color_bgc %>"><%= color_text %></div>
                </div>
                <form class="likeBtn" action="" method="post" name="likeForm" onsubmit="return like()">
                    <input type="hidden" name="clothesSid" value="<%= sid %>">
                    <div class="likeBSubmit <%= sid==which && belong==member ? "liked" : "" %>">
                        <i class="fas fa-heart"></i>
                    </div>
                </form>
            </div>
        </div>`;
    var clothes_template = _.template(clothes_str);

    var pageCon = $('.pageCon');
    var pageCon_str=`<a class="clothesPage <%= page==i ? 'active' : '' %>" href="javascript:selectPage(<%= i %>)"><%= i %></a>`
    var pageCon_template = _.template(pageCon_str);
// <!-- -------------------------------------------------------------------------------------------->

        $(".selectorForm").submit() // 進入頁面時載入資料

        var clothesSid = "" // 宣告回傳的變數(浴衣的sid)
        
        // 收藏API連線與回傳
        function like() {
            $.post("php_api/clothes_like_api.php",{"clothesSid":clothesSid}, function(data){ 
                if(data.success){
                    $(".successText").text(data.info);
                    $(".success").fadeIn().delay(300).fadeOut();
                    setTimeout(function(){
                        $(".selectorForm").submit()
                    },400)
                }else{
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(500).fadeOut();
                }
            },"json") // 以JSON字串回傳
            return false
        }
        
        //選擇頁碼
        function selectPage(page){
            $(".pageInput").val(page);
            goSelect()
            $(".pageInput").val(1)
        }

        //篩選API連線與回傳
        function goSelect() {
            $.ajax({
                url: "php_api/select_api.php",     
                type: "POST",
                cache:false,
                dataType: 'json',
                data:$(".selectorForm").serialize(),
                success:function(data) {
                    var page = data.page;
                    var totalPages = data.totalPages;
                    pageCon.html('');
                    for(var i=1; i<=totalPages; i++){
                        pageCon.append(pageCon_template({i:i, page:page}))
                    }

                    clothes_container.html('');
                    for(var i=0; i< data.rows.length; i++){
                        var item=data.rows[i],size_bgc,size_text,color_bgc,color_text;
                        switch (item.size){
                            case "w":size_bgc="#DB4D6D";size_text="女";break;
                            case "m":size_bgc="#4A5861";size_text="男";break;
                            case "k":size_bgc="#F58032";size_text="童";break;
                        }
                        switch (item.color){
                            case "r":color_bgc="#CB4042";color_text="赤";break;
                            case "y":color_bgc="#F05E1C";color_text="茶";break;
                            case "g":color_bgc="#838A2D";color_text="綠";break;
                            case "b":color_bgc="#0089A7";color_text="青";break;
                            case "p":color_bgc="#77428D";color_text="藤";break;
                            case "bk":color_bgc="#080808";color_text="墨";break;
                        }

                    var clothes_array={
                        "sid":item.sid,
                        "size":item.size,
                        "color":item.color,
                        "size_bgc":size_bgc,
                        "size_text":size_text,
                        "color_bgc":color_bgc,
                        "color_text":color_text,
                        "small_photo":item.small_photo,
                        "big_photo":item.big_photo,
                        "belong":item.belong,
                        "member":data.member_sid,
                        "which":item.which
                    }
                    clothes_container.append(clothes_template(clothes_array));
                    }
                },
            });
            return false
        };

        // 收藏按鈕點擊事件
        $("html").on("click",".likeBSubmit",function(){
            <?php if(isset($_SESSION['user'])){?>
                // 有登入的話把這個浴衣的sid設定到變數中
                clothesSid = $(this).prev("input").val() 
                $(this).addClass("liked")
                // 送出表單
                $(this).parent().submit()
                
            <?php }else{ ?>
                // 沒登入的話出現對話框 及登入器
                $(".errorText").text("收藏功能需登入才能使用");
                $(".error").fadeIn().delay(1000).fadeOut();
                $(".bgBlur").fadeIn();
            <?php }; ?>
        });


        // 旗幟點擊事件
        $(".pattern").click(function(){
            var colorNum = $(this).index();
            var onOff = $(".c-selector").eq(colorNum+1).prop("checked")
            $(".c-selector").eq(colorNum+1).prop("checked",!onOff)
            $("#c-all").prop("checked",false)
            $(this).toggleClass("chosen")
            $(".selectorForm").submit()

            if($(".c-selector:checked").length==0){
                $("#c-all").prop("checked",true)
            }
        })

        // 控制"全部"按鈕的與其他類別按鈕的選取邏輯
        $(".z-selector").not("#z-all").change(function(){
            $("#z-all").prop("checked",false)
        })
        $("#z-all").change(function(){
            $(this).siblings().prop("checked",false)
        })
        // 控制"全部"按鈕的與其他類別按鈕的選取邏輯 以及顏色類別紐與旗幟的關係
        $(".c-selector").not("#c-all").change(function(){
            var colorNum = $(this).index()
            $(".pattern").eq(colorNum/2-1).toggleClass("chosen")
            $("#c-all").prop("checked",false)
        })
        $("#c-all").change(function(){
            $(".pattern").removeClass("chosen")
            $(this).siblings().prop("checked",false)
        })


        // 控制全部按鈕的與其他類別按鈕的選取邏輯
        $(".z-selector").change(function(){
            $(".selectorForm").submit()
            if($(".z-selector:checked").length==0){
                $("#z-all").prop("checked",true)
            }
        })
        $(".c-selector").change(function(){
            $(".selectorForm").submit()
            if($(".c-selector:checked").length==0){
                $("#c-all").prop("checked",true)
            }
        })

        $().fancybox({
            selector : '.clothes a:visible'
        });

    </script>
</body>
</html>