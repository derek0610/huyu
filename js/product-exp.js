// -------------------------------------------product----------------------------------------
        var productBtn = $(".product-nav li");
        var productCon = $(".content");
        let windowWidth = $(window).width()
    
        productBtn.click(function(){
            $(this).addClass("p-active").siblings().removeClass("p-active");
            productCon.eq($(this).index()).fadeIn().siblings().fadeOut();
        })
        
// -------------------------------------------comment----------------------------------------        
        var commentBtn = $(".comment-btn");
        function autoGrow(textarea){
        var adjustedHeight=textarea.clientHeight;
            adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
            if (adjustedHeight>textarea.clientHeight){
                textarea.style.height=adjustedHeight+'px';
            };
        };

        $(".comment-form").slideUp();
        
        $(".radio").change(function(){
                $(this).prevAll(".rating-star").children().removeClass("no-rate");
                $(this).nextAll(".rating-star").children().addClass("no-rate");
        });

        $("body").on("click",".commentPage",function(){
            $('html').animate({scrollTop : $(".commentCon").offset().top-200},500)
        })
// -----------------------------------------photo-group----------------------------------------- 
        var photoGroup = $(".photo-group");
        $('.photo-group').slick({
            dots: true,
            infinite: true,
            speed: 700,
            fade: true,
            cssEase: 'linear',
            autoplay: true,
            autoplaySpeed: 2200,
            arrows: false,
        });
// -----------------------------------------reserve----------------------------------------- 
        var reserve = $(".reserve");
        var reserveBox = $(".reserve-box");
        var close = $(".close-symbol");
        var peopleNum = $("#people").val();
        var makeupNum = $("#makeup").val();

        reserve.click(function(){
            reserveBox.fadeIn()
        });
        close.click(function(){
            reserveBox.fadeOut();
        })

        $.datetimepicker.setLocale('zh-TW');
        $('#datetimepicker').datetimepicker({
            timepicker:false,
            format:'Y.m.d',
            inline:true,
            defaultSelect: false,
            defaultDate: false,
            minDate:'+1970/01/05',
            maxDate:'+1970/04/05',
            yearStart:'2019',
            yearEnd:'2030',
            lang:'zh-TW',
            todayButton:false,
            scrollMonth:false,
            scrollInput:false,
        });

        $(".add").click(function(){
            peopleNum++
            if(peopleNum>=15){
                peopleNum=15
                $(this).css("background","#ccc")
                $("#people").val(peopleNum)
            }else{
                $(".less").css("background","#8FCBD9")
                $("#people").val(peopleNum) 
            }
            if(peopleNum>makeupNum){
                $(".makeup-add").css("background","#8FCBD9")
            }
        });

        if(peopleNum<=1){
            peopleNum=1
            $(this).css("background","#ccc")
            $("#people").val(peopleNum)
        }
        $(".less").click(function(){
            peopleNum--
            if(peopleNum<=1){
                peopleNum=1
                $(this).css("background","#ccc")
                $("#people").val(peopleNum)
            }else{
                $(".add").css("background","#8FCBD9")
                $("#people").val(peopleNum) 
            }
            if(peopleNum<makeupNum){
                makeupNum=peopleNum
                $("#makeup").val(makeupNum)
            }
        });
        $(".makeup-add").click(function(){
            makeupNum++
            if(makeupNum>0){
                $(".makeup-less").css("background","#8FCBD9")
            }
            if(makeupNum>=peopleNum){
                makeupNum=peopleNum
                $(this).css("background","#ccc")
                $("#makeup").val(makeupNum)
            }else{
                $(".makeup-less").css("background","#8FCBD9")
                $("#makeup").val(makeupNum) 
            }
        });
        if(makeupNum<=0){
            makeupNum=0
            $(this).css("background","#ccc")
            $("#makeup").val(makeupNum)
        }
        $(".makeup-less").click(function(){
            makeupNum--
            if(makeupNum<peopleNum){
                $(".makeup-add").css("background","#8FCBD9")
            }
            if(makeupNum<=0){
                makeupNum=0
                $(this).css("background","#ccc")
                $("#makeup").val(makeupNum)
            }else{
                $(".makeup-add").css("background","#8FCBD9")
                $("#makeup").val(makeupNum) 
            }
        });


        
        function calc() {
            var perPrice = $(".plan-radio:checked").data("price")
            var peopleNum = $("#people").val()
            var makeupPrice = $("#makeup").data("price")
            var makeupNum = $("#makeup").val()
            var totalPrice = "NT$"+(perPrice*peopleNum+makeupPrice*makeupNum)
            
                
            $("#total").val(totalPrice.slice(0,totalPrice.length-3)+","+totalPrice.slice(totalPrice.length-3,totalPrice.length))
        }
        $(".plan-radio").change(function(){           
            calc()
        })
        $(".people-select>div").click(function(){
            calc()
        })
        $(".makeup-select>div").click(function(){
            calc()
        })
 
