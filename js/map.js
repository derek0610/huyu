$(".site-market").show()
$(".site").not(".site-market").hide()
$('img[usemap]').rwdImageMaps()

var scrollBarBox = $(".scrollBarBox")
var scrollBar = $(".scrollBar")
var siteDisplay = $(".siteDisplay")
var siteGroup = $(".siteGroup")
if(siteGroup.prop('scrollHeight')==Math.round(siteDisplay.height())){$(scrollBarBox).hide()}else{$(scrollBarBox).show()}
scrollBar.css("height",Math.round(scrollBarBox.height()/siteGroup.prop('scrollHeight')*siteDisplay.height()))

function barHeight() {
    var scrollBarBox = $(".scrollBarBox")
    var scrollBar = $(".scrollBar")
    var siteDisplay = $(".siteDisplay")
    var siteGroup = $(".siteGroup")
    if(siteGroup.prop('scrollHeight')==Math.round(siteDisplay.height())){$(scrollBarBox).hide()}else{$(scrollBarBox).show()}
    scrollBar.css("height",Math.round(scrollBarBox.height()/siteGroup.prop('scrollHeight')*siteDisplay.height()))
}

$(window).resize(function(){
    var scrollBarBox = $(".scrollBarBox")
    var scrollBar = $(".scrollBar")
    var siteDisplay = $(".siteDisplay")
    var siteGroup = $(".siteGroup")
    if(siteGroup.prop('scrollHeight')==Math.round(siteDisplay.height())){$(scrollBarBox).hide()}else{$(scrollBarBox).show()}
    scrollBar.css("height",Math.round(scrollBarBox.height()/siteGroup.prop('scrollHeight')*siteDisplay.height()))
})

$(".area").on({
    click: function(){
        var mapNum = $(this).data("index")
        var siteName = $(this).text()
        $(this).addClass("mapPick").children(".navDot").addClass("dotShow")
        $(this).siblings().removeClass("mapPick").children(".navDot").removeClass("dotShow")
        $(this).parent().siblings().children().removeClass("mapPick").children(".navDot").removeClass("dotShow")
        $(".mapSite").eq(mapNum-1).addClass("show").siblings(".mapSite").removeClass("show")
        $(".areaName h2").text(siteName)

        if($(this).data("index")==1){
            $(".site-market").show()
            $(".site").not(".site-market").hide()
        }else if($(this).data("index")==2){
            $(".site-park").show()
            $(".site").not(".site-park").hide()
        }else if($(this).data("index")==3){
            $(".site-new").show()
            $(".site").not(".site-new").hide()
        }else if($(this).data("index")==4){
            $(".site-hot").show()
            $(".site").not(".site-hot").hide()
        }
        barHeight()
        $(".siteDisplay").scrollTop(0)
    },
    mouseenter: function(){
        var mapNum = $(this).data("index")
        $(this).addClass("mapHover")
        $(".mapSite").eq(mapNum-1).addClass("hover")
    },
    mouseleave: function(){
        var mapNum = $(this).data("index")
        $(this).removeClass("mapHover")
        $(".mapSite").eq(mapNum-1).removeClass("hover")
    }
})


function colorChange(color){
    $(".mapName p").css("background",color)
    $(".mapName span").css("background",color)
}


$("area").on({
    click: function(){
        var mapNum = $(this).index()
        var siteName = $(".area").eq(mapNum).text()
        $(".area").eq(mapNum).addClass("mapPick").children(".navDot").addClass("dotShow")
        $(".area").eq(mapNum).siblings(".area").removeClass("mapPick").children(".navDot").removeClass("dotShow")
        $(".area").eq(mapNum).parent().siblings().children().removeClass("mapPick").children(".navDot").removeClass("dotShow")
        $(".mapSite").eq(mapNum).addClass("show").siblings(".mapSite").removeClass("show")
        $(".areaName h2").text(siteName)

        if($(this).index()==0){
            $(".site-market").show()
            $(".site").not(".site-market").hide()
        }else if($(this).index()==1){
            $(".site-park").show()
            $(".site").not(".site-park").hide()
        }else if($(this).index()==2){
            $(".site-new").show()
            $(".site").not(".site-new").hide()
        }else if($(this).index()==3){
            $(".site-hot").show()
            $(".site").not(".site-hot").hide()
        }
        barHeight()
        $(".siteDisplay").scrollTop(0)
    },
    mouseenter: function(){
        var mapNum = $(this).index()
        $(".area").eq(mapNum).addClass("mapHover")
        $(".mapSite").eq(mapNum).addClass("hover")
        $(".mapName p").text($(".area.mapHover").text())
        switch($(".mapName p").text()){
            case "北投市場周邊" :
                colorChange("#569EA6");
                break;
            case "北投公園周邊" :
                colorChange("#94B86E");
                break;
            case "中新新村周邊" :
                colorChange("#4A5861");
                break;
            case "地熱谷周邊" :
                colorChange("#F58032");
                break;
        }
        $(".mapName").show()
    },
    mouseleave: function(){
        var mapNum = $(this).index()
        $(".area").eq(mapNum).removeClass("mapHover")
        $(".mapSite").eq(mapNum).removeClass("hover")
        $(".mapName").hide()
    },
    mousemove: function(e){
        var mX = e.pageX
        var mY = e.pageY
        $(".mapName").css({
            top: e.pageY-60,
            left: e.pageX-20,
        })
    }
})

var scrollBarBox = $(".scrollBarBox")
var scrollBar = $(".scrollBar")
var siteDisplay = $(".siteDisplay")
var siteGroup = $(".siteGroup")
if(siteGroup.prop('scrollHeight')==Math.round(siteDisplay.height())){$(scrollBarBox).hide()}else{$(scrollBarBox).show()}
scrollBar.css("height",Math.round(scrollBarBox.height()/siteGroup.prop('scrollHeight')*siteDisplay.height()))

$(".siteDisplay").scroll(function(){
    var siteScrollTop = $(this).scrollTop()
    var scrollScrollTop = Math.round(scrollBarBox.height()/siteGroup.prop('scrollHeight')*siteScrollTop)
    scrollBar.css("top",scrollScrollTop)
})

var lastM = $(".site-market").length-1
var lastP = lastM+$(".site-park").length
var lastN = lastP+$(".site-new").length
var lastH = lastN+$(".site-hot").length
$(".site").eq(lastM).css("margin",0)
$(".site").eq(lastP).css("margin",0)
$(".site").eq(lastN).css("margin",0)
$(".site").eq(lastH).css("margin",0)


var omY
var ostY
var mDown = function(event){
    omY = event.pageY
    ostY = siteDisplay.scrollTop()
    $("body").on("mouseup",mUp)
    $("body").on("mousemove",mMove)
}
var mUp = function(event){
    $("body").off("mouseup",mUp)
    $("body").off("mousemove",mMove)
}
var mMove = function(event){
    var bmY = event.pageY
    var dist = ostY + (bmY - omY)*5
    siteDisplay.scrollTop(dist)
};
scrollBarBox.on('mousedown', mDown);