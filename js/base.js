var menuBtn = $(".mobile-menu");
var memberBtn = $(".member");
var loginWindows = $(".bgBlur");
var windowClose = $(".windowClose")
let navItem = $(".web-navItem")
// ---------------------拿到這頁的網址-------------------------
var pageHref
        $(document).ready(function(){
            pageHref = location.href
        })
// ---------------------開關navbar-------------------------
        menuBtn.click(function(){
            $("nav").css("transform","translate(0,0)");
        });
        $(".menu-close").click(function(){
            $("nav").css("transform","translate(-100%,0)");
        });
// ---------------------改善navbar奇怪的換行-------------------------
        if($(window).height()<=886 && $(window).height()>680){
            for(i=0;i<5;i++){
                navItem.eq(i).html(navItem.eq(i).text().slice(0,2)+"<br/>"+navItem.eq(i).text().slice(2,4))
            }
        }else{
            for(i=0;i<5;i++){
                navItem.eq(i).html(navItem.eq(i).text())
            }
        }
// ---------------------手機板navbar的收合-------------------------
        $(window).resize(function(){
            let windowWidth = $(window).outerWidth()
            let windowHeight = $(window).outerHeight()
            
            if(windowWidth>768){
                $("nav").css("transform","translate(0,0)");
            };
            if(windowWidth<768){
                $("nav").css("transform","translate(-100%,0)");
            };
            if(windowHeight<=886 && windowHeight>680){
                for(i=0;i<5;i++){
                    navItem.eq(i).html(navItem.eq(i).text().slice(0,2)+"<br/>"+navItem.eq(i).text().slice(2,4))
                }
            }else{
                for(i=0;i<5;i++){
                    navItem.eq(i).html(navItem.eq(i).text())
                }
            }
        });
// ---------------------開關登入器-------------------------
        memberBtn.click(function(){
            loginWindows.fadeIn()
        });

        windowClose.click(function(){
            loginWindows.fadeOut()
        });
// ---------------------登入和註冊的切換-------------------------
        $("#logIn").on("click",function(){
            $(this).addClass("windowSelect").siblings().removeClass("windowSelect");
            $(".windowTitle").text("會員登入");
            $(".loginForm").show();
            $(".signForm").hide();
        });

        $("#signIn").on("click",function(){
            $(this).addClass("windowSelect").siblings().removeClass("windowSelect");
            $(".windowTitle").text("註冊會員");
            $(".signForm").show();
            $(".loginForm").hide();
        });

// ---------------------註冊的表單檢查-------------------------
        var $signUsername = $('#signUsername');
        var $signAccount = $('#signAccount');
        var $signPassword = $('#signPassword');
        var $signBirthday = $('#signBirthday');
        var signFields = [$signUsername, $signAccount, $signPassword, $signBirthday];
        
        var birthdayRegex = /^\d{4}-\d{2}-\d{2}$/

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }
        
        function signIn(){
            
            signFields.forEach(function(val){
                val.prev().find(".signErro").hide();
            });

            var isPass = true;

            if($signUsername.val().length < 2) {
                isPass = false;
                $signUsername.prev().find(".signErro").show()
            }
            
            if(! validateEmail($signAccount.val())) {
                isPass = false;
                $signAccount.prev().find(".signErro").show()
            }

            if($signPassword.val().length < 6) {
                isPass = false;
                $signPassword.prev().find(".signErro").show()
            }

            if(!birthdayRegex.test($signBirthday.val())) {
                isPass = false;
                $signBirthday.prev().find(".signErro").show()
            }

            if(isPass){
                $.post('php_api/signIn_api.php', $("#signForm").serialize(), function(data){
                    if(data.success){
                        $(".successText").text(data.info);
                        $(".success").fadeIn().delay(800).fadeOut();
                        $("#logIn").addClass("windowSelect").siblings().removeClass("windowSelect");
                        $(".windowTitle").text("會員登入");
                        $(".loginForm").show();
                        $(".signForm").hide();
                    }else{
                        $(".errorText").text(data.info);
                        $(".error").fadeIn().delay(800).fadeOut();
                    }
                }, "json")
            }

            return false;
        }
// ---------------------登入的表單檢查-------------------------
        var $loginAccount = $('#loginAccount');
        var $loginPassword = $('#loginPassword');
        var loginFields = [$signUsername, $signAccount, $signPassword, $signBirthday];
        
        function login(){
            
            loginFields.forEach(function(val){
                val.prev().find(".notice").hide();
            });

            var isPass = true;
            
            if(! validateEmail($loginAccount.val())) {
                isPass = false;
                $loginAccount.prev().find(".notice").show()
            }

            if($loginPassword.val().length < 6) {
                isPass = false;
                $loginPassword.prev().find(".notice").show()
            }

            if(isPass){
                $.post('php_api/login_api.php', $("#loginForm").serialize(), function(data){
                    if(data.success){
                        $(".successText").text(data.info)
                        $(".success").fadeIn().delay(800).fadeOut();
                        setTimeout(function(){location.reload()},1000)
                    }else{
                        $(".errorText").text(data.info)
                        $(".error").fadeIn().delay(800).fadeOut();
                    }
                }, "json")
            }

            return false;
        }


    function myConfirm(msg) {
        var df = $.Deferred();  
        var confirmWindow = $("<div id='c'></div>");
        confirmWindow.html($(".c").html())

        .on("click", "input", function() {
            confirmWindow.fadeOut(300)
            setTimeout(function(){
                confirmWindow.remove()
            },300)
            if (this.value == "確定"){
                df.resolve();
            }else{
                df.reject();
            }
        })
        .find(".confirmText").text(msg);
            $("body").append(confirmWindow);
            return df.promise();
    }
      