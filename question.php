<?php
require __DIR__. '/php_api/__db_connect.php';
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 常見問題</title>
    <link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/question.css">

<?php $reserveGuide = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <header>
            <h1 class="title">常見問題</h1>
        </header>
        <section>
            <form action="" method="" name="searchForm" class="searchCon" onsubmit="return false">
                <div class="typeBtnGroup">
                    <input type="radio" name="qType" value="" id="all" class="qType" checked>
                    <label for="all">全部問題</label>

                    <input type="radio" name="qType" value="member" class="qType" id="member">
                    <label for="member">會員相關</label>

                    <input type="radio" name="qType" value="reserve" class="qType" id="reserve">
                    <label for="reserve">預約相關</label>

                    <input type="radio" name="qType" value="journey" class="qType" id="journey">
                    <label for="journey">行程相關</label>

                    <input type="radio" name="qType" value="order" class="qType" id="order">
                    <label for="order">訂單相關</label>

                    <label id="empty"></label>
                </div>
                <div class="searchBar">
                    <input type="text" name="keyword" class="searchText" placeholder="輸入關鍵字搜尋">
                    <div class="searchBtn"><i class="fas fa-search"></i></div>
                </div>
            </form><hr>
            <div class="questCon">
                
            </div>
        </section>
    </div>
    
<?php require __DIR__.'/__html_js.php'?>

    <script src="js/lodash.js"></script>
    <script>
        $("html").on({
            mouseenter:function(){
                $(this).next().find("h4").addClass('hover')
            },
            mouseleave:function(){
                $(this).next().find("h4").removeClass('hover')
            },
            click:function(){
                $(this).next().find("h4").toggleClass('active')
                $(this).closest('.quest').siblings().find("h4").removeClass('active')
                $(this).next().find(".answer").slideToggle()
                $(this).closest('.quest').siblings().find(".answer").slideUp()
            }
        },".questIcon")

        $("html").on({
            mouseenter:function(){
                $(this).find("h4").addClass('hover')
            },
            mouseleave:function(){
                $(this).find("h4").removeClass('hover')
            },
            click:function(){
                $(this).find("h4").toggleClass('active')
                $(this).closest('.quest').siblings().find("h4").removeClass('active')
                $(this).next().slideToggle()
                $(this).closest('.quest').siblings().find(".answer").slideUp()
            }
        },".questTitle")

        $(".searchText").on({
            focus: function(){
                $(".searchBar").css("border","2px solid #94B86E")
                $(".searchBtn").css("background","#94B86E")
            },
            blur: function(){
                $(".searchBar").css("border","2px solid #F58032")
                $(".searchBtn").css("background","#F58032")
            }
        })


        var questCon = $(".questCon")
        var quest_str = `
        <div class="quest">
            <div class="questCon">
                <div class="questTitle"><h1 class="questIcon">Q</h1><h4><%= question %></h4></div>
                <div class="answer">
                    <p class="answerText"><%= answer %></p>
                </div>
            </div>
        </div>`; 
        var quest_fn = _.template(quest_str);

        function search() {
            $.post('php_api/question_api.php',$('.searchCon').serialize(),function(data){
                questCon.html(""); 
                for(i=0; i<data.rows.length; i++){
                    var array={
                        question: data.rows[i]['question'],
                        answer: data.rows[i]['answer']
                    }
                    questCon.append(quest_fn(array));
                }
            },"json")
        }
        
        search()

        $('.qType').change(function(){
            $('.searchText').val("")
            search()
        })

        $('.searchBar').keydown(function(e){
            if(e.which == 13){
                $('#all').prop("checked",true)
                search()
            }
        })

        $('.searchBtn').click(function(){
            $('#all').prop("checked",true)
            search()
        })
    </script>
</body>
</html>