<?php
require __DIR__. '/php_api/__db_connect.php';
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 浴衣漫遊-散步方案</title>
    <link rel="stylesheet" href="css/product-select-ver2.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <div class="linebox">
            <div class="vline"></div>
            <div class="circle"><i class="fas fa-arrows-alt-h"></i><p class="after">滑桿</p><p class="before">拖曳</p></div>
        </div>

        <div class="rightCon">
            <img class="conBG" src="images/Beitou3.jpg" alt="">
            <div class="context">
                <header class="product-intro">
                    <h1 class="title white">浴衣漫遊</h1>
                    <p class="intro">到北投玩別再只是泡溫泉了!「忽浴」提供多樣豐富的體驗方式讓你身穿日式浴衣悠遊北投，趣味闖關、深度之旅、輕鬆自由行，挑個適合你的旅遊方案吧!</p>
                </header>
                <section class="conBox">
                    <div class="selectCon">
                        <a href="product-trip-history.php" class="type">
                            <img src="images/Beitou3.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">軼聞路線</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要走進歷史，文藝風情的「軼聞路線」將帶你見證北投的變遷。</p>
                            </div>
                        </a>
                        <a href="product-trip-explore.php" class="type">
                            <img src="images/game2.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">探索路線</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要來場冒險遊戲，「探索路線」中有許多秘密等著你發掘。</p>
                            </div>
                        </a>
                    </div>
                    <div class="nameCon r-nameCon">
                        <h2 class="name white">浴衣<br>小旅行</h2>
                        <h2 class="mobile-name">浴衣小旅行</h2>
                        <p class="nameIntro">想聽聽「巷子內」才知道的北投小故事嗎？「忽浴」有最專業、活潑的導覽員，無論是自然景觀、歷史人文，甚至當地傳說都難不倒他們喔。</p>
                    </div>
                </section>
            </div>
        </div>

        <div class="leftCon">
            <img class="conBG" src="images/Portfolio8.jpg" alt="">
            <div class="context" >
                <header class="product-intro left-t">
                    <h1 class="title white">浴衣漫遊</h1>
                    <p class="intro">到北投玩別再只是泡溫泉了!「忽浴」提供多樣豐富的體驗方式讓你身穿日式浴衣悠遊北投，趣味闖關、深度之旅、輕鬆自由行，挑個適合你的旅遊方案吧!</p>
                </header>
                <section class="conBox">
                    <div class="nameCon">
                        <h2 class="name white">浴衣<br>體驗</h2>
                        <h2 class="mobile-name">浴衣體驗</h2>
                        <p class="nameIntro">「忽浴」提供完善的日式浴衣換裝體驗，讓漫步在北投溫泉街的您，不用遠赴日本也能穿著正統日式浴衣，用不同方式感受北投的人文風情。</p>
                    </div>
                    <div class="selectCon">
                        <a href="product-exp-walk.php" class="type">
                            <img src="images/female2.jpg" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">散步方案</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要輕鬆一點，「散步方案」可以讓你自由地走在北投的古道上。</p>
                            </div>
                        </a>
                        <a href="product-exp-photo.php" class="type">
                            <img src="images/linda8.png" alt="">
                            <div class="typeText">
                                <h3 class="typeTitle">攝影方案</h3>
                                <div class="line"></div>
                                <p class="typeIntro">如果想要紀錄美好的回憶，「寫真方案」能夠幫你封存旅行中的快樂。</p>
                            </div>
                        </a>
                    </div>
                </section>
            </div>
        </div>
    </div>

<?php require __DIR__.'/__html_js.php'?>
    
    <script>
        var windowW
        var linebox = $(".linebox")
        var leftCon = $(".leftCon")
        var right = $(".rightCon .selectCon")
        var left = $(".leftCon .selectCon")
        var rightBG = $(".rightCon .conBG")
        var leftBG = $(".leftCon .conBG")

        var mDown = function(event){
            windowW = $(".container").width()
            windowHW = (windowW)/2
            $("body").on("mouseup",mUp)
            $("body").on("mousemove",mMove)
            $("body").on("mouseleave",mLeave)
        }
        var mUp = function(event){
            $("body").off("mouseup",mUp)
            $("body").off("mousemove",mMove)
        }
        var mLeave = function(event){
            $("body").off("mouseup",mUp)
            $("body").off("mousemove",mMove)
        }
        var mMove = function(event){
            var bmX = event.pageX
            var dist = bmX/windowW*100
            clipSave = "polygon(0 0,"+dist+"% 0,"+dist+"% 100%,0 100%)"
            var lineboxNP = linebox.offset().left+25
            var lineboxPre = (lineboxNP-windowHW)/(windowW-windowHW)
            var leftMax = $("nav").width()
            
            linebox.css("left","calc("+dist+"% - 25px)")
            leftCon.css("clip-path",clipSave)
            left.css({"opacity":0.2+lineboxPre,"visibility":"visible"})
            right.css({"opacity":1.2-lineboxPre,"visibility":"visible"})
            rightBG.css("filter","brightness("+(70-dist)+"%) blur("+(0.5-lineboxPre*2)+"px)")
            leftBG.css("filter","brightness("+(-30+dist)+"%) blur("+(0.5+lineboxPre*2)+"px)")
            $(".after").css("opacity",1-Math.abs(lineboxPre))
            $(".before").css("opacity",1-Math.abs(lineboxPre))
        };
        linebox.on('mousedown', mDown);
        
        $(window).resize(function(){
            var windowWidth = $(window).outerWidth()
            if(windowWidth<1024){
                $(".selectCon").css("opacity",1)
                leftCon.css("clip-path","none")
            }else{
                $(".selectCon").css("opacity",0)
                leftCon.css("clip-path",clipSave)
            }
        })
    </script>
</body>
</html>