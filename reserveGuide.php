<?php
require __DIR__. '/php_api/__db_connect.php';
require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 預約指南</title>
    <link rel="stylesheet" href="css/reserveGuide.css">

<?php $reserveGuide = ""?>
<?php require __DIR__.'/__html_body.php'?>
    
<!-- --------------------------------------header----------------------------------- -->
    <div class="headerImg" data-300-start="opacity:1;" data-800-start="opacity:0.3;"></div>
    <div data-0="background-position:0px 0px;" data-5000="background-position:100px-1000px;" class="BG-01">
    <div data-0="background-position:0px 0px;" data-5000="background-position:100px+600px;" class="BG-02">
    <div data-0="background-position:0px 0px;"  data-5000="background-position:100px-800px;" class="BG-03">
    <div class="container">
        <header>
            <h1 class="title" data-start="top:0vw;" data-600-start="top:30vw;">預約指南</h1>
        </header>
        <a class="question" href="question.php"><span><i class="fas fa-question-circle"></i></span><h5>有其他疑問嗎?<br>看看常見問題!</h5></a>
<!-- ------------------------------------------------------------------------------- -->
        <div class="searchBar">
            <ul class="searchBar_ul">
                <li class="searchBar_li"><p>01&nbsp</p><p>浴衣漫遊</p></li>
                <li class="searchBar_li"><p>02&nbsp</p><p>相冊衣覽</p></li>
                <li class="searchBar_li"><p>03&nbsp</p><p>北投尋訪</p></li>
                <li class="searchBar_li"><p>04&nbsp</p><p>關於我們</p></li>
                <li class="searchBar_li"><p>05&nbsp</p><p>到店體驗</p></li>
            </ul>
        </div>
        <section>
            <article>
                <div class="stepText">
                    <div class="stepTitle invisible">
                        <h1 class="stepNum">01</h1>
                        <h1 class="stepName">浴衣漫遊</h1></div>
                    <div class="stepCon invisible">
                        <h4 class="littleTitle">「浴衣體驗」</h4>
                        <p>道地的日式和服體驗，輕鬆散步或是寫真紀錄，留下美好回憶。<br/>-</p>
                        <h4 class="littleTitle">「浴衣小旅行」</h4>
                        <p>導覽員帶你深度探索，拉近了北投與我們之間的距離。</p>
                        <div class="stepFoot">
                            <a href="product-select.php">前往參與→</a>
                        </div>
                    </div>
                </div>
                <div class="stepImg">
                    <img src="images/reserveGuide/mainpic02.png" alt="">
                </div>
            </article>
            <hr class="indraw">
            <article class="reverse">
                <div class="stepText">
                    <div class="stepTitle invisible">
                        <h1 class="stepNum">02</h1>
                        <h1 class="stepName">相冊衣覽</h1></div>
                    <div class="stepCon invisible">
                        <p>可事先查看租借的浴衣花色，在到店時並告知我們的同仁，盡可能地讓您速速穿上並出發去體驗！<br/>-</p>
                        <p>瀏覽過往的旅客寫真，關於他們的足跡，並也留下屬於你的一筆～</p>
                        <div class="stepFoot">
                            <a href="album-clothes.php">前往欣賞→</a>
                        </div>
                    </div>
                </div>
                <div class="stepImg">
                    <img src="images/reserveGuide/mainpic03.png" alt="">
                </div>
            </article>
            <hr class="indraw">
            <article>
                <div class="stepText">
                    <div class="stepTitle invisible">
                        <h1 class="stepNum ">03</h1>
                        <h1 class="stepName">北投尋訪</h1></div>
                    <div class="stepCon invisible">
                        <p>想要規劃一趟北投之旅卻不知道該如何開始嗎?我們有需多推薦的景點與店家喔，快來看看詳細資訊吧！</p>
                        <div class="stepFoot">
                            <a href="map.php">前往探索→</a>
                        </div>
                    </div>
                </div>
                <div class="stepImg">
                    <img src="images/reserveGuide/mainpic04.png" alt="">
                </div>
            </article>
            <hr class="indraw">
            <article class="reverse">
                <div class="stepText">
                    <div class="stepTitle invisible">
                        <h1 class="stepNum">04</h1>
                        <h1 class="stepName">關於我們</h1></div>
                    <div class="stepCon invisible">
                        <p>出發前，不妨再確認一下確切位置及交通資訊，順道聽聽我們訴說，關於當初成立的那份初衷。</p>
                        <div class="stepFoot">
                            <a href="aboutUs.php">前往了解→</a>
                        </div>
                    </div>
                </div>
                <div class="stepImg">
                    <img src="images/reserveGuide/mainpic01.png" alt="">
                </div>
            </article>
            <hr class="indraw">
            <article>
                <div class="stepText">
                    <div class="stepTitle invisible">
                        <h1 class="stepNum">05</h1>
                        <h1 class="stepName">到店體驗</h1></div>
                    <div class="stepCon invisible">
                        <ul>
                            <li><p>a. 順利地抵達。</p></li>
                            <li><p>b. 告知小夥伴您的預約資訊。</p></li>
                            <li><p>c. 挑選浴衣款式，若已在網路上選好，可以跟我們說喔!</p></li>
                            <li><p>d. 北投之旅，啟程！</p></li>
                        </ul>
                    </div>
                </div>
                <div class="stepImg">
                    <img src="images/reserveGuide/mainpic05.png" alt="">
                </div>
            </article>
        </section>
    </div>
    </div>
    </div>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script src="js/skrollr.min.js"></script>
    <script>

        var windowWidth = $(window).width()
        var lastScrollTop

        if(windowWidth>1024){
            var s = skrollr.init({
            forceHeight: false,
            });
        }
        
        $(window).scroll(function () {

            $('.invisible').each(function (i) {
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {
                    $(this).removeClass('invisible').addClass('fadeInUp');
                }
            });

            $('.infadeIn').each(function (i) {
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {
                    $(this).removeClass('infadeIn').addClass('fadeIn');
                }
            });

            $('.indraw').each(function (i) {
                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                if (bottom_of_window > bottom_of_object) {
                    $(this).removeClass('indraw').addClass('draw');
                }
            });

            var scrollTop = $(window).scrollTop()
            if(scrollTop>lastScrollTop){
                $(".question").css("transform","translate(125px,0)");
            }else{
                $(".question").css("transform","translate(0,0)");
            }
            lastScrollTop = scrollTop
        });

        $(".question").on({
            mouseenter: function(){
                $(this).css("transform","translate(0,0) ")
            },
            mouseleave: function(){
                $(this).css("transform","translate(125px,0)")
            },
        })


        $('.searchBar_li').click(function(){
            var witch = $(this).index()
            $('html').animate({ scrollTop: $('article').eq(witch).offset().top-100}, 800);
        });
    </script>
</body>
</html>