<?php 
require __DIR__. '/php_api/__db_connect.php';

// 沒有登入時回到首頁
if(! isset($_SESSION['user'])){
    header('Location: index.php');
    exit;
};

$member_sid = $_SESSION['user']['sid'];

$c_sql = "SELECT `clothes_like`.*,`clothes`.`size`,`clothes`.`color`,`clothes`.`small_photo`,`clothes`.`big_photo` FROM `clothes_like` LEFT JOIN `clothes` ON `clothes_like`.`which`=`clothes`.`sid` WHERE `clothes_like`.`belong`=$member_sid";
$c_stmt = $pdo->query($c_sql);
$c_row = $c_stmt->fetchAll();

$a_sql = "SELECT `area_like`.*,`area`.`name`,`area`.`type`,`area`.`address`,`area`.`phone`,`area`.`tel_href`,`area`.`open`,`area`.`photo`,`area`.`intro`,`area`.`map` FROM `area_like` LEFT JOIN `area` ON `area_like`.`which`=`area`.`sid` WHERE `area_like`.`belong`=$member_sid";
$a_stmt = $pdo->query($a_sql);
$a_row = $a_stmt->fetchAll();

require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 會員中心</title>
    <link rel="stylesheet" href="css/member.css">

<?php $member = ""?>
<?php require __DIR__.'/__html_body.php'?>
<!-- --------------------------------------header----------------------------------- -->
    <div class="container">
        <header>
            <h1 class="title">會員中心</h1>
        </header>
<!-- -------------------------------------member-nav------------------------------------------ -->
        <div class="member-nav">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
                <a href="member-likeList.php" class="memberBtn select"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i> 常見問題</a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i> 後臺管理</a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i> 登出</a>
        </div>

        <div class="member-nav_mobile">
            <?php if($_SESSION['user']['sid']!=1){ ?>
                <a href="member-travelManage.php" class="memberBtn"><i class="fas fa-fw fa-suitcase"></i></a>
                <a href="member-likeList.php" class="memberBtn select"><i class="fas fa-fw fa-heart"></i></a>
                <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
                <a href="question.php" class="memberBtn"><i class="fas fa-fw fa-question"></i></a>
            <?php }else{ ?>
                <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-fw fa-cog" aria-hidden="true"></i></a>
            <?php } ?>
            <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-fw fa-door-open"></i></a>
        </div>
<!-- -------------------------------------likeList------------------------------------------ -->
        <section class="likeList">
            <h2 class="listTitle">著物收藏</h2>
            <hr>
            <ul class="clothes">
                <?php if(empty($c_row)){echo '<p class="empty">您目前還沒有收藏任何浴衣，快到<a href="album-clothes.php">「著物展示」</a>去看看吧！</p>';} ?>
                <?php foreach($c_row as $r):?>
                <li class="item c_item">
                    <div class="clothesPhoto"><img src="images/clothes/<?= $r['small_photo'] ?>" alt=""></div>
                    <div class="deleCon">
                        <a href="javascript: delete_c(<?= $r['sid'] ?>)" class="dele"><i class="fas fa-trash-alt"></i> 取消收藏</a>
                    </div>
                </li>
                <?php endforeach;?>
            </ul>
            <h2 class="listTitle">景點收藏</h2><hr>
            <?php if(empty($a_row)){echo '<p class="empty">您目前還沒有收藏任何景點，快到<a href="map.php">「北投尋訪」</a>去看看吧！</p>';}else{
                echo '<p class="remind"><span><i class="fas fa-bell"></i></span>點擊圖片看更多細節</p>';
            } ?>
            <ul class="viewpoint">
                <?php foreach($a_row as $r):?>
                <li class="item a_item">
                    <div class="viewPhoto"><img src="images/viewpoint/<?= $r['photo'] ?>" alt=""></div>
                    <div class="viewInfo">
                        <h3 class="storeName"><?= $r['name'] ?></h3><hr>
                        <div class="storeInfo">
                            <div class="infoItem">
                                <span class="infoIcon"><i class="fas fa-map-marker-alt"></i></span>
                                <a class="infoText address"><?= $r['address'] ?></a>
                            </div>
                            <div class="infoItem">
                                <span class="infoIcon"><i class="fas fa-phone-alt"></i></span>
                                <a class="infoText phoneNum" href="<?= $r['tel_href'] ?>"><?= $r['phone'] ?></a>
                            </div>
                            <div class="infoItem">
                                <span class="infoIcon"><i class="fas fa-clock"></i></span>
                                <p class="infoText"><?= $r['open'] ?></p>
                            </div>
                        </div>
                        <div class="deleCon">
                            <a href="javascript: delete_a(<?= $r['sid'] ?>)" class="dele"><i class="fas fa-trash-alt"></i> 取消收藏</a>
                        </div>
                        <p class="introText" style="display:none"><?= $r['intro'] ?></p>
                        <p class="mapText" style="display:none"><?= $r['map'] ?></p>
                    </div>
                </li>
                <?php endforeach;?>

            </ul>
        </section>
        <div class="siteWindowCon" style="display:none">
            <div class="siteWindow">
                <div class="siteWindowClose"><i class="fas fa-times fa-fw"></i></div>
                <div class="siteWindowContent">
                    <div class="siteWindowImg">
                        <img src="" alt="">
                    </div>
                    <div class="siteWindowTitle">
                        <h3 class="siteWindowName"></h3>
                        <hr>
                    </div>
                    <ul class="siteWindowInfo">
                        <li><span><i class="fas fa-map-marker-alt"></i></span><a></a></li>
                        <li><span><i class="fas fa-phone-alt"></i></span><a class="phoneNum" href="02-2896-0626"></a></li>   
                        <li><span><i class="fas fa-clock"></i></span><p></p></li><hr>
                        <li><p></p></li>
                    </ul>
                    <iframe src="" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script>
        function delete_c(sid){
            myConfirm("確定要取消收藏嗎?")
            .done(function(){
                $(".confirm").fadeOut(300)
                setTimeout(function(){
                    location.href = 'php_api/c_like_dele.php?sid=' + sid;
                },300)
            })
            .fail(function(){
                
            });
        }

        function delete_a(sid){
            myConfirm("確定要取消收藏嗎?")
            .done(function(){
                $(".confirm").fadeOut(300)
                setTimeout(function(){
                    location.href = 'php_api/a_like_dele.php?sid=' + sid;
                },300)
            })
            .fail(function(){
                
            });
        }


        $(".viewPhoto").click(function(){
            let siteImg = $(this).find("img").attr("src")
            let siteName = $(this).next(".viewInfo").find(".storeName").text()
            let siteInfo = $(this).next(".viewInfo").find(".storeInfo").html()
            let introText = $(this).next(".viewInfo").find(".introText").text()
            let mapText = $(this).next(".viewInfo").find(".mapText").text()

            let intro = $("<li></li>")
            intro.html("<p>"+introText+"</p>")

            $(".siteWindowImg img").attr("src",siteImg);
            $(".siteWindowName").text(siteName)
            $(".siteWindowInfo").html(siteInfo)
            $(".siteWindowInfo").append("<hr>")
            $(".siteWindowInfo").append(intro)
            $("iframe").attr("src",mapText)

            $(".siteWindowCon").fadeIn()
        })

        $(".siteWindowClose").click(function(){
            $(".siteWindowCon").fadeOut()
        })

    </script>
</body>
</html>