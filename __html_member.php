<header>
        <h1 class="title">會員中心</h1>
</header>
<div class="member-nav">
    <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i> 行程管理</a>
    <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i> 收藏清單</a>
    <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i> 會員資料</a>
    <a href="question.php" class="memberBtn"><i class="fas fa-question"></i> 常見問題</a>
    <?php if($_SESSION['user']['sid']==1): ?>
    <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-cog" aria-hidden="true"></i> 後臺管理</a>
    <?php endif; ?>
    <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-door-open"></i> 登出</a>
</div>
<div class="member-nav_mobile">
    <a href="member-travelManage.php" class="memberBtn select"><i class="fas fa-fw fa-suitcase"></i></a>
    <a href="member-likeList.php" class="memberBtn"><i class="fas fa-fw fa-heart"></i></a>
    <a href="member-profile.php" class="memberBtn"><i class="fas fa-fw fa-user"></i></a>
    <a href="question.php" class="memberBtn"><i class="fas fa-question"></i></a>
    <?php if($_SESSION['user']['sid']==1): ?>
    <a href="member-travelManage_master.php" class="memberBtn"><i class="fa fa-cog" aria-hidden="true"></i></a>
    <?php endif; ?>
    <a href="./php_api/logout_api.php" class="memberBtn"><i class="fas fa-door-open"></i></a>
</div>