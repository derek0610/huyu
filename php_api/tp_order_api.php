<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 0,
    'info' => '',
    'postData' => [],
];

if(!isset($_POST)){
    $result['code'] = 444;
    $result['info'] = '請選擇您想參加的方案';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

$_SESSION['tpOrder'] = [
    "type" => $_POST['type'],
    "date" => $_POST['date'],
    "time" => $_POST['time'],
    "plan" => $_POST['plan'],
    "people" => $_POST['people'],
    "makeup" => $_POST['makeup'],
    "total" => $_POST['total'],
]; 

if(!isset($_SESSION['user'])){
    $result['code'] = 555;
    $result['info'] = '請先登入再進行預約';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
};

$result['code'] = 1;
$result['postData'] = $_POST;

// print_r($_SESSION['tpOrder']);

echo json_encode($result, JSON_UNESCAPED_UNICODE);