<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '發生錯誤'
];
if(isset($_POST)){
    $sql="INSERT INTO `reaction`(`email`, `theme`, `time`, `text`) VALUES (?,?,?,?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        $_POST['cs'][0]['value'],
        $_POST['cs'][1]['value'],
        $_POST['csTime'],
        $_POST['cs'][2]['value']
    ]);
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 1;
        $result['info'] = '您的意見已成功送出<br>我們會盡快回復您';
    }else{
        $result['code'] = 410;
        $result['info'] = '請重新填寫';
    }
}
echo json_encode($result, JSON_UNESCAPED_UNICODE);
?>