<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 000,
    'info' => '發生錯誤',
    'postData' => [],
];

if(isset($_SESSION['user']) && isset($_POST)){
    $result['postData']=$_POST;
    $memberNum = $_SESSION['user']['sid'];
    $orderNum = $_POST['order_number'];

    $o_sql = "SELECT * FROM `orders` WHERE 1 AND `belong`=$memberNum AND `order_number`=$orderNum";
    $o_stmt = $pdo->query($o_sql);
    $o_row = $o_stmt->fetch();

    $sql = "INSERT INTO `comments`(`order_number`, `type`, `name`, `star`, `text`, `time`) VALUES (?,?,?,?,?,?)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        $orderNum,
        $o_row['type'],
        $_SESSION['user']['name'],
        $_POST['star'],
        $_POST['text'],
        date("Y-m-d")
    ]);
    
    if($stmt->rowCount()==1){
        $u_sql = "UPDATE `orders` SET `comment`= 1 WHERE 1 AND `belong`=$memberNum AND `order_number`=$orderNum";
        $pdo->query($u_sql);

        $result['success'] = true;
        $result['code'] = "111";
        $result['info'] = "成功加入評論";
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);