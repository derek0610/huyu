<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '',
    'postData' => [],
];

if(!isset($_POST['orderNum'])){
    $result['code']="000";
    $result['info']="發生錯誤";
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

$result['postData']=$_POST;

$sql= "SELECT * FROM `orders` WHERE 1 AND `belong`=? AND `order_number`=?";
$stmt = $pdo->prepare($sql);
$stmt->execute([
    $_SESSION['user']['sid'],
    $_POST['orderNum']
]);
$r = $stmt->fetch();

if($r['comment']==0){
    $result['success'] = true;
    $result['code'] = "111";
    $result['info'] = "成功";
    $result['orderNum'] = $r['order_number'];
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

$result['code']="000";
$result['info']="此筆行程已完成評論";
echo json_encode($result, JSON_UNESCAPED_UNICODE);