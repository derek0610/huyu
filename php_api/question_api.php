<?php 
require __DIR__.'/__db_connect.php';

$type = !empty($_POST['qType']) ? $_POST['qType'] : 0;
$keyword = !empty($_POST['keyword']) ? implode("%' OR `keyword` LIKE '%",str_split($_POST['keyword'],3)) : 0;
    
$result =[
    'type' => $type,
    'keyword' => $keyword,
    'postData' => $_POST,
    'rows' => [],
];

$where = " WHERE 1";
if(!empty($type)){
    $where.=" AND `type` = '$type'";
};
if(!empty($keyword)){
    $where.=" AND `keyword` LIKE '%$keyword%'";
};

$sql = "SELECT * FROM `question`".$where;
$stmt = $pdo->query($sql);
$rows = $stmt->fetchAll();
$result['rows'] = $rows;

echo json_encode($result,JSON_UNESCAPED_UNICODE);