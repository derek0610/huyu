<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '發生錯誤',
    'postData' => [],
];

if(isset($_POST['orderNum']) and isset($_SESSION['user'])){
    $result['postData'] = $_POST;
    $member_sid = $_SESSION['user']['sid'];
    $orderNum = $_POST['orderNum'];

    $sql = "UPDATE `orders` SET `pay_status`=?,`order_status`=? WHERE 1 AND `order_number`=? AND `belong`=?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        "已退費",
        "已取消",
        $orderNum,
        $member_sid,
    ]);

    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 1;
        $result['info'] = '退費成功並取消預約';
    }else {
        $result['code'] = 0;
        $result['info'] = '發生錯誤';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);