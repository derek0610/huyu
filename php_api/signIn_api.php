<?php
require __DIR__. '/__db_connect.php';


$result = [
    'success' => false,
    'code' => 400,
    'info' => '',
    'post' => $_POST,
];

$s_sql = "SELECT 1 FROM `members` WHERE `email`=?";
$s_stmt = $pdo->prepare($s_sql);
$s_stmt->execute([ $_POST['signAccount'] ]);

if($s_stmt->rowCount() >= 1){
    $result['code'] = 420;
    $result['info'] = '此電子信箱已註冊';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

// 去掉頭尾空白, 然後轉小寫
$email = strtolower(trim($_POST['signAccount']));
// 密碼編碼, 不要明碼
$password = sha1(trim($_POST['signPassword']));

$stmt = $pdo->prepare("INSERT INTO `members`(`name`, `birthday`,`email`,`password`) VALUES (?,?,?,?)");

$stmt->execute([
    $_POST['signUsername'],
    $_POST['signBirthday'],
    $email,
    $password,
]);

if($stmt->rowCount()){
    $result['success'] = true;
    $result['info'] = '註冊成功';
}else{
    $result['info'] = '未成功註冊';
};

echo json_encode($result); // 傳轉換成JSON字串 