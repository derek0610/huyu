<?php 
require __DIR__.'/__db_connect.php';

    $result =[
        'success' => false,
        'code' => 400,
        'info' => '',
        'POSTData' => [],
    ];

$s_sql = "SELECT 1 FROM `clothes_like` WHERE `which`=? AND `belong`=?";
    $s_stmt = $pdo->prepare($s_sql);
    $s_stmt->execute([ $_POST['clothesSid'],$_SESSION['user']['sid'] ]);

    if($s_stmt->rowCount() >= 1){
    $result['code'] = 420;
    $result['info'] = '此浴衣已在收藏清單';
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

    if(isset($_POST['clothesSid']) and isset($_SESSION['user'])){
        
        $result['POSTData'] = $_POST;

        $like_sql = ("INSERT INTO `clothes_like`(`belong`, `which`) VALUES (?,?)");
        $stmt = $pdo->prepare($like_sql);
        $stmt->execute([ $_SESSION['user']['sid'],$_POST['clothesSid'] ]);
    
        if($stmt->rowCount()==1){
            $result['success'] = true;
            $result['code'] = 200;
            $result['info'] = '加入收藏';
    
        } else {
            $result['code'] = 410;
            $result['info'] = '發生錯誤';
        }
    };

    echo json_encode($result, JSON_UNESCAPED_UNICODE);