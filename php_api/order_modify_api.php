<?php
require __DIR__. '/__db_connect.php';
$member_sid = $_SESSION['user']['sid'];

$result =[
    'success' => false,
    'code' => 400,
    'info' => '發生錯誤',
    'postData' => [],
];

if(isset($_SESSION['user']) && isset($_POST)){
    $orderNum = $_POST['orderNum'];
    $modifyTime = $_POST['modifyTime'];
    $travelerNew = $_POST['travelerNew'];
    
    $sql = "UPDATE `orders` SET `modify`=?,`modify_time`=? WHERE `order_number`=?"; 
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        1,
        $modifyTime,
        $orderNum,
    ]);//此區目前是正常的

    $i=0;
    $iswork=0;

    foreach( $travelerNew as $row ){
        $od_sql = "UPDATE orders_details SET `name`=?, `identification`=?,`mobile`=?,`email`=? WHERE sid IN ( SELECT * FROM ( SELECT sid FROM orders_details 
        where `order_number`=? ORDER BY sid ASC LIMIT ?, 1 )tmp)";
        
        $od_stmt = $pdo->prepare($od_sql);
        $od_stmt->execute([
            $row[0]['value'],
            $row[1]['value'],
            $row[2]['value'],
            $row[3]['value'],
            $orderNum,
            $i
        ]);
        if($od_stmt->rowCount()>=0){
            $iswork+=1;
            $i+=1;
        }
    };
    if($iswork==count($travelerNew)){
        $result['success'] = true;
        $result['code'] = 1;
        $result['rows'] = $iswork;
        $result['info'] = '修改成功';
    }else{
        $result['code'] = 410;
        $result['rows'] = $iswork;
        $result['info'] = '訂單未成立';
    };
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);