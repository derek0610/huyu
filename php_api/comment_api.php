<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '',
    'postData' => [],
];

if(!isset($_SESSION['user'])){
    $result['code']="444";
    $result['info']="評論功能需登入才能使用";
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

if(!isset($_POST['type'])){
    $result['code']="000";
    $result['info']="發生錯誤";
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

$result['postData']=$_POST;

$sql= "SELECT * FROM `orders` WHERE 1 AND `belong`=? AND `type`=?";
$stmt = $pdo->prepare($sql);
$stmt->execute([
    $_SESSION['user']['sid'],
    $_POST['type']
]);

if($stmt->rowCount()==0){
    $result['code']="000";
    $result['info']="您需要體驗過此行程才能評論";
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit;
}

while($r = $stmt->fetch()){
    if($r['comment']==0 && $r['order_status']=='已完成'){
        $result['success'] = true;
        $result['code'] = "111";
        $result['info'] = "成功";
        $result['orderNum'] = $r['order_number'];
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }
}
$result['code']="000";
$result['info']="您的行程均已完成評論";
echo json_encode($result, JSON_UNESCAPED_UNICODE);