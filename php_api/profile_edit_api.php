<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
    'postData' => [],
];


if(isset($_POST['username']) and isset($_POST['password']) and isset($_SESSION['user'])){
    $result['postData'] = $_POST;

    // 密碼編碼, 不要明碼
    $oldPassword = trim($_SESSION['user']['password']);
    $password = sha1(trim($_POST['password']));

    $sql = "UPDATE `members` SET `name`=?,`birthday`=?,`password`=?,`mobile`=? WHERE `email`=? AND `password`=?";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([
        $_POST['username'],
        $_POST['birthday'],
        $password,
        $_POST['mobile'],
        $_SESSION['user']['email'],
        $oldPassword
    ]);

    // 影響的列數 (筆數)
    if($stmt->rowCount()==1){
        $result['success'] = true;
        $result['code'] = 200;
        $result['info'] = '資料更新成功';

        $_SESSION['user']['name'] = $_POST['username'];
        $_SESSION['user']['birthday'] = $_POST['birthday'];
        $_SESSION['user']['password'] = $password;
        $_SESSION['user']['mobile'] = $_POST['mobile'];
    } else {
        $result['code'] = 410;
        $result['info'] = '您沒有更新任何資料';
    }
}

echo json_encode($result, JSON_UNESCAPED_UNICODE);