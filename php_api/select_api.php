<?php 
require __DIR__.'/__db_connect.php';

$member_sid = isset($_SESSION['user']) ? $_SESSION['user']['sid'] : "";

if(isset($_POST['page'])){
    $page = intval($_POST['page']);
    $per_page = 12;
    $star = ($page-1)*$per_page;

    $result =[
        'member_sid' => $member_sid,
        'page' => $page,
        'per_page' => $per_page,
        'totalRows' => 0,
        'totalPages' => 0,
        'postData' => $_POST,
        'rows' => [],
    ];
};

$where = " WHERE 1";
$size = isset($_POST['size']) ? $_POST['size'] : "";
$color = isset($_POST['color']) ? $_POST['color'] : "";
$sizeVal = is_array($size) ? implode("','",$size) : $size;
$colorVal = is_array($color) ? implode("','",$color) : $color;

if(!empty($sizeVal)){
    $where.=" AND `size` IN ('$sizeVal')";
};
if(!empty($colorVal)){
    $where.=" AND `color` IN ('$colorVal')";
};

$t_sql = "SELECT COUNT(1) FROM `clothes`".$where;
$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];
$result['totalRows'] = $totalRows;

$totalPages = ceil($totalRows/$per_page);
$result['totalPages'] = $totalPages;


$sql = "SELECT * FROM `clothes`".$where." ORDER BY `clothes`.`sid` ASC LIMIT $star, $per_page";

if(!empty($member_sid)){
    $sql = ("SELECT `clothes`.*,`clothes_like`.`belong`, `which` FROM `clothes` LEFT JOIN `clothes_like` ON `clothes`.`sid`=`clothes_like`.`which` AND `clothes_like`.`belong`=$member_sid".$where." ORDER BY `clothes`.`sid` ASC LIMIT $star, $per_page");
}

$stmt = $pdo->query($sql);
$rows=$stmt->fetchAll();
$result['rows'] = $rows;


echo json_encode($result,JSON_UNESCAPED_UNICODE);
