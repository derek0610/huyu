<?php
require __DIR__. '/__db_connect.php';

$result =[
    'success' => false,
    'code' => 400,
    'info' => '參數不足',
];

if(isset($_POST) and isset($_SESSION['tpOrder']) and isset($_SESSION['user'])){
    $result['postData'] = $_POST;

    $order_number = strtotime("now").$_SESSION['user']['sid'];

    $o_in_sql = "INSERT INTO `orders`(`belong`, `order_number`, `date`, `time`, `type`, `plan`, `people`, `extra`, `total`) VALUES (?,?,?,?,?,?,?,?,?)";
    $stmt = $pdo->prepare($o_in_sql);
    $stmt->execute([
        $_SESSION['user']['sid'],
        $order_number,
        $_SESSION['tpOrder']['date'],
        $_SESSION['tpOrder']['time'],
        $_SESSION['tpOrder']['type'],
        $_SESSION['tpOrder']['plan'],
        $_SESSION['tpOrder']['people'],
        $_SESSION['tpOrder']['makeup'],
        $_SESSION['tpOrder']['total'],
    ]);

    $order_sid = $pdo->lastInsertId();
    $_SESSION['tpOrder']['orderNum'] = $order_sid;

    if($stmt->rowCount()==1){
        $iswork = 0;
        foreach( $_POST as $row ){
            $od_sql = "INSERT INTO `orders_details`(`order_number`, `name`, `identification`, `mobile`, `email`) VALUES (?,?,?,?,?)";
            $od_stmt = $pdo->prepare($od_sql);
            $od_stmt->execute([
                $order_number,
                $row[0]['value'],
                $row[1]['value'],
                $row[2]['value'],
                $row[3]['value'],
            ]);
            if($od_stmt->rowCount()==1){
                $iswork+=1;
            }
        };
        if($iswork==count($_POST)){
            $result['success'] = true;
            $result['code'] = 1;
            $result['rows'] = $iswork;
            $result['info'] = '預約成功';
        }else{
            $result['code'] = 410;
            $result['rows'] = $iswork;
            $result['info'] = '訂單未成立';
        };
    }
}
echo json_encode($result, JSON_UNESCAPED_UNICODE);