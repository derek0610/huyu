<?php 
require __DIR__.'/__db_connect.php';

if(isset($_POST['page'])){
    $page = $_POST['page'];
    $type = $_POST['type'];
    $per_page = 10;
    $star = ($page-1)*$per_page;

    $result =[
        'page' => $page,
        'type' => $type,
        'per_page' => $per_page,
        'totalRows' => 0,
        'totalPages' => 0,
        'postData' => $_POST,
        'rows' => [],
    ];

    $t_sql = "SELECT COUNT(1) FROM `comments` WHERE 1 AND `type` = '$type'";
    $totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];
    $result['totalRows'] = $totalRows;

    $totalPages = ceil($totalRows/$per_page); // 總頁數
    $result['totalPages'] = $totalPages;

    $sql = "SELECT * FROM `comments` WHERE 1 AND `type` = '$type' ORDER BY `time` DESC LIMIT $star, $per_page";
    $stmt = $pdo->query($sql);
    $rows=$stmt->fetchAll();
    $result['rows'] = $rows;
}

echo json_encode($result,JSON_UNESCAPED_UNICODE);

