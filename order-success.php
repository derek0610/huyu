<?php 
require __DIR__. '/php_api/__db_connect.php';

if(!isset($_SESSION['user']) || !isset($_SESSION['tpOrder'])){
    header('Location: index.php');
    exit;
}
$order_sid = $_SESSION['tpOrder']['orderNum'];
$o_sql = "SELECT * FROM `orders` WHERE 1 AND `sid`= $order_sid";
$o_stmt = $pdo->query($o_sql);
$o_row = $o_stmt->fetch();
$order_number = $o_row['order_number'];


$d_sql = "SELECT COUNT(1) FROM `orders_details` WHERE 1 AND `order_number`= $order_number";
$totalRows = $pdo->query($d_sql)->fetch(PDO::FETCH_NUM)[0];
$totalOther = $totalRows-1;

$m_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1";
$m_stmt = $pdo->query($m_sql);
$m_row = $m_stmt->fetch();

$ot_sql = "SELECT * FROM `orders_details` WHERE 1 AND `order_number`= $order_number LIMIT 1 , $totalOther";
$ot_stmt = $pdo->query($ot_sql);
$ot_row = $ot_stmt->fetchAll();

$account = "1470 3050 5443 39".$_SESSION['user']['sid'];
$tomorrow = new DateTime('tomorrow');
$payDay = $tomorrow->format("Y-m-d");
$dayFormat = $tomorrow->format("Y")."年".$tomorrow->format("m")."月".$tomorrow->format("j")."日23點59分以前";

$p_sql = "INSERT INTO `pay_info`(`order_number`,`pay_date`,`deadline`,`atm_account`) VALUES (?,?,?,?)";
$p_stmt = $pdo->prepare($p_sql);
$p_stmt->execute([
    $order_number,
    $payDay,
    $dayFormat,
    $account,
])
?>

<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 預約結帳</title>
    <link rel="stylesheet" href="css/order-success.css">

<?php $product = ""?>
<?php require __DIR__.'/__html_body.php'?>

    <div class="container">
        <section>
            <div class="payCon">
                <div class="explain bg-green"><h3 class="white"><i class="fas fa-check-circle"></i> 預約成功/尚未付款</h3></div>
                <div class="basicBox">
                    <p class="mg-b">感謝您預約我們的行程，您的訂單資訊已確認送出!</p>
                    <p class="mg-b">以下為您的購物明細和「轉帳帳號」資訊，並於繳款期限 <strong class="red"><?= $dayFormat ?></strong> 利用任一銀行或郵局的ATM自動提款機轉帳，輸入下方轉帳帳號及金額，就能完成付款!</p>
                    <p >轉帳完成後30分鐘，您可至「會員中心-我的行程」查詢您的訂單狀況是否為「已付款」。</p>
                </div>
                <div class="explain bg-orange"><h3 class="white"><i class="fas fa-info-circle"></i> 轉帳資料</h3></div>
                <ul class="basicBox">
                    <p class="red">提醒您，若您於繳款期限到期仍未轉帳，您的訂單將被取消。</p><hr>
                    <li>銀行代碼：<p>013國泰世華銀行</p></li>
                    <li>轉帳帳號：<p><?= $account ?></p></li> 
                    <li>轉帳金額：<p><?= $o_row['total'] ?></p></li> 
                    <li>繳款期限：<p><?= $tomorrow->format("Y"); ?>年<?= $tomorrow->format("m"); ?>月<?= $tomorrow->format("j"); ?>日23點59分以前</p></li>
                </ul>
                <div class="explain flex"><p>有任何疑問歡迎聯繫我們！</p><p>門市電話：02-2369-3366</p><p>服務時間：AM09:30-PM18:30</p></div>
            </div>
        </section>
        <section>
            <div class="stepTittle">
                <h2 class="stepText">行程細節</h2>
            </div>
            <div class="detailCon">
                <div class="detailTitle">
                    <h3><?php 
                        switch($o_row['type']){
                        case "walk":echo "浴衣體驗-散步方案";break;
                        case "photo":echo "浴衣體驗-攝影方案";break;
                        case "explore":echo "浴衣小旅行-探險路線";break;
                        case "history":echo "浴衣小旅行-軼聞路線";break; 
                    }?></h3>
                    <p class="orderNum">訂單編號:<?= $order_number ?></p>
                </div>
                <hr>
                <ul class="orderInfo">
                    <li><p class="infoTag">日期</p><p class="infoText"><?= $o_row['date'] ?></p></li>
                    <li><p class="infoTag">時間</p><p class="infoText"><?= $o_row['time']=="am" ? "上午 10:00" : "下午 14:00"  ?></p></li>
                    <li><p class="infoTag">人數</p><p class="infoText"><?= $o_row['people'] ?>人</p></li>
                </ul><hr>
                <ul class="orderInfo">
                    <li>
                        <p class="infoTag">方案</p><p class="infoText"><?php 
                            switch($o_row['plan']){
                                case  "inside":echo "棚內拍攝/每人NT$1,000";break;
                                case  "outside":echo "外景拍攝/每人NT$1,600";break;
                                case  "in_out":echo "棚拍+外拍/每人NT$2,000";break;
                                case  "two":echo "兩小時體驗/每人NT$500";break;
                                case  "four":echo "四小時體驗/每人NT$700";break;
                                case  "six":echo "六小時體驗/每人NT$850";break;
                                case  "twoFour":echo "二至四人/每人NT$2,000";break;
                                case  "fiveNine":echo "五至九人/每人NT$1,800";break;
                                case  "tenUp":echo "十人以上/每人NT$1,500";break;
                            }?>
                        </p>
                    </li>
                    <?php if($o_row['extra']>0):?>
                        <li><p class="infoTag">妝髮</p><p class="infoText"><?= $o_row['extra'] ?>人/每人 NT$500</p></li>
                    <?php endif;?>
                </ul><hr>
                <div class="orderInfo flex-end">
                    <p class="infoTag">總金額</p><h3 class="orderTotal"><?= $o_row['total'] ?></h3>
                </div>
            </div>
        </section>
        <section>
            <div class="stepTittle">
                <h2 class="stepText">旅客資料</h2>
            </div>
            <div class="stepCon">
                <div class="boxTitle"><h3>團員1(主要聯絡人)</h3></div>
                <div class="basicBox">
                    <div class="itemGroup">
                        <div class="formItem">
                            <h5 class="label">真實姓名</h5>
                            <p class="textInput"><?= $m_row['name'] ?></p>
                        </div>
                        <div class="formItem">
                            <h5 class="label">身分證字號</h5>
                            <p class="textInput"><?= $m_row['identification'] ?></p>
                        </div>
                        <div class="formItem">
                            <h5 class="label">行動電話</h5>
                            <p class="textInput"><?= $m_row['mobile'] ?></p>
                        </div>
                        <div class="formItem">
                            <h5 class="label">電子信箱</h5>
                            <p class="textInput" id="mailInput"><?= $m_row['email'] ?></p>
                        </div>
                    </div>
                    
                </div>
                <?php if($ot_stmt->rowCount()>0):?>
                <div class="boxTitle"><h3>同行團員</h3></div>
                <?php $i=1 ?>
                <?php foreach($ot_row as $row):?>
                <?php $i+=1 ?>
                <div class="basicBox">
                    <h3>團員<?= $i ?></h3>
                    <div class="itemGroup">
                        <div class="formItem">
                            <h5 class="label">真實姓名</h5>
                            <p class="textInput"><?= $row['name']?></p>
                        </div>
                        <div class="formItem"> 
                            <h5 class="label">身分證字號</h5>
                            <p class="textInput"><?= $row['identification']?></p>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
                <?php endif;?>
                <div class="explain"><p>若旅客資訊有誤，您可以至會員中心的行程管理中進行修改線上修改，所有資訊均可於行程出發日72小時修改。</p></div>
            </div>
            <button class="okAndBack">確認並返回會員中心</button>
        </section>
    </div>
    
<?php require __DIR__.'/__html_js.php'?>

    <script>
        $(".okAndBack").on("click",function(){
            <?php unset($_SESSION['tpOrder']) ?>
            location.href = 'member-travelManage.php'
        })
    </script>
</body>
</html>