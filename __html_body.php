</head>
<body>
        <div class="mobile-menuCon"><div class="mobile-menu"><i class="fas fa-bars"></i></div><div class="menuAfter"></div><div class="menuBefore"></div></div>

        <nav>
            <a class="logoBox" href="./"><div class="logo"></div></a>
            
            <a href="member-travelManage.php" class="member <?= isset($member) ? 'active' : '' ?>">會員中心</a> 
            <button class="member" style="<?= isset($_SESSION['user']) ? 'display:none' : '' ?>">登入/註冊</button>
            
            <ul class="web-nav">
                <li class="aboutUs"><a href="aboutUs.php" class="web-navItem <?= isset($aboutUs) ? 'active' : '' ?>">關於我們</a></li>
                <li class="reserveGuide"><a href="reserveGuide.php" class="web-navItem <?= isset($reserveGuide) ? 'active' : '' ?>">預約指南</a></li>
                <li class="product"><a href="product-select.php" class="web-navItem <?= isset($product) ? 'active' : '' ?>">浴衣漫遊</a></li>
                <li class="album"><a href="album-select.php" class="web-navItem <?= isset($album) ? 'active' : '' ?>">相冊衣覽</a></li>
                <li class="map"><a href="map.php" class="web-navItem <?= isset($map) ? 'active' : '' ?>">北投巡訪</a></li>
                <li class="menu-close"><a><i class="fas fa-arrow-left"></i></a></li>
            </ul>
        </nav>
        <div class="success" style="display:none">
            <div class="successInfo">
                <div class="successIcon"><i class="fas fa-check-circle"></i></div>
                <p class="successText"></p>
            </div>
        </div>
        <div class="error" style="display:none">
            <div class="errorInfo">
                <div class="errorIcon"><i class="fas fa-exclamation-circle"></i></div>
                <p class="errorText"></p>
            </div>
        </div>
        <div class="c" style="display:none">
            <div class="confirm">
                <div class="confirmInfo">
                    <div class="confirmIcon"><i class="fas fa-question-circle"></i></div>
                    <p class="confirmText"></p>
                    <div class="confirmBtn">
                        <input type="button" value="確定" class="confirmYes">
                        <input type="button" value="取消" class="confirmNo">
                    </div>
                </div>
            </div>
        </div>
        <div class="bgBlur" style="display:none">
            <div class="windowCon">
                <div class="windowNav">
                    <div class="switch">
                        <span id="logIn" class="windowSelect">我要登入</span>
                        <span id="signIn">我要註冊</span>
                    </div>
                    <div class="windowClose"><i class="fas fa-times fa-fw"></i></div>
                </div>
                <div class="memberWindow">
                    <h2 class="windowTitle">會員登入</h2>
                    <form class="loginForm" action="" name="loginForm" id="loginForm" onsubmit="return login()" method="post">

                        <fieldset class="loginGroup">
                            <label for="loginAccount"><p>帳號(電子信箱)</p><span class="notice">請輸入正確格式</span></label>
                            <input type="text" name="loginAccount" id="loginAccount" placeholder="請輸入您的帳號" autocomplete="username">
                        </fieldset>

                        <fieldset class="loginGroup">
                            <label for="loginPassword"><p>密碼</p><span class="notice">請輸入至少6個英文或數字</span></label>
                            <input type="password" name="loginPassword" id="loginPassword" placeholder="請輸入您的密碼" autocomplete="current-password">
                        </fieldset>

                        <fieldset class="btnGroup">
                            <input type="submit" class="loginSubmit" value="登 入">

                            <button class="fbLogin"><i class="fab fa-facebook-f"></i> Facebook帳號登入</button>
                            <button class="googleLogin"><i class="fab fa-google"></i> Google帳號登入</button>
                        </fieldset>
                        <div class="forget"><a href="">忘記帳號?</a><a href="">忘記密碼?</a></div>

                    </form>
                    <form class="signForm" action="" style="display:none" name="signForm" id="signForm" onsubmit="return signIn()" method="post">

                        <fieldset class="signGroup">
                            <label for="signUsername"><p>真實姓名</p><span class="signErro">請輸入真實姓名</span></label>
                            <input type="text" name="signUsername" id="signUsername" placeholder="請輸入真實姓名">
                        </fieldset>

                        <fieldset class="signGroup">
                            <label for="signAccount"><p>帳號(電子信箱)</p><span class="signErro">請輸入正確格式</span></label>
                            <input type="text" name="signAccount" id="signAccount" placeholder="電子信箱即為您的帳號" autocomplete="username">
                        </fieldset>

                        <fieldset class="signGroup">
                            <label for="signPassword"><p>密碼</p><span class="signErro">請輸入至少6個英文或數字</span></label>
                            <input type="password" name="signPassword" id="signPassword" placeholder="請輸入至少6個英文或數字" autocomplete="current-password">
                        </fieldset>

                        <fieldset class="signGroup">
                            <label for="signBirthday"><p>生日</p><span class="signErro">請輸入正確的日期格式</span></label>
                            <input type="text" name="signBirthday" id="signBirthday" placeholder="1911-01-01">
                        </fieldset>

                        <fieldset class="signBtnGroup">
                            <input type="submit" class="signSubmit" value="註 冊">
                        </fieldset>
                        
                    </form>
                </div>
            </div>
        </div>

        
