<?php require __DIR__.'/php_api/__db_connect.php';

$member_sid = isset($_SESSION['user']) ? $_SESSION['user']['sid'] : "";
$sql = "SELECT * FROM `area` WHERE 1";
if(!empty($_SESSION['user']['sid'])){
    $sql = "SELECT `area`.*,`area_like`.`belong`, `which` FROM `area` LEFT JOIN `area_like` ON `area`.`sid`= `area_like`.`which` AND `area_like`.`belong`=$member_sid ORDER BY `area`.`sid` ASC";
}
$stmt = $pdo->query($sql);

?>

<?php require __DIR__.'/__html_head.php'?>

    <title>忽浴 | 北投尋訪</title>
    <link rel="stylesheet" href="css/map.css">

<?php $map = ""?>
<?php require __DIR__.'/__html_body.php'?>

<!-- --------------------------------------mapCon----------------------------------- -->
    <div class="container">
        <main>
            <section class="mapCon">
                    <header class="product-intro">
                            <h1 class="title">北投尋訪</h1>
                            <p class="intro">1895 年日本人來到台灣，將北投發展成他們在台灣的第二個故鄉，一百二十年的歷史帶來了怎麼樣的羈絆與豐富的文化底蘊呢？穿著浴衣一探究竟吧!</p>
                        </header>
                <div class="actTitle"><h2>選擇區域</h2></div>
                <div class="navigator">
                    <ul>
                        <!-- <li class="navDot"></li> -->
                        <li class="area mapPick" data-index="1"><span class="navDot dotShow"><i class="fas fa-circle"></i></span><p>北投市場周邊</p></li>
                        <li class="area" data-index="2"><span class="navDot"><i class="fas fa-circle"></i></span><p>北投公園周邊</p></li>
                    </ul>
                    <ul>
                        <li class="area" data-index="3"><span class="navDot"><i class="fas fa-circle"></i></span><p>中新新村周邊</p></li>
                        <li class="area" data-index="4"><span class="navDot"><i class="fas fa-circle"></i></span><p>地熱谷周邊</p></li>
                    </ul>
                </div>
                <div class="mapName"><span></span><p>北投市場周邊</p></div>
                <article class="mapImg">
                    <div class="originMap"><img src="images/map/map.png" alt=""></div>
                    <div class="mapSite show" data-index="1"><img src="images/map/market.png" alt=""></div>
                    <div class="mapSite" data-index="2"><img src="images/map/park.png" alt=""></div>
                    <div class="mapSite" data-index="3"><img src="images/map/new.png" alt=""></div>
                    <div class="mapSite" data-index="4"><img src="images/map/hot.png" alt=""></div>
                    <div class="mapSite mapTop"><img src="images/map/mapTop.png" alt="" usemap="#Map" width="800" height="560">
                    <map name="Map" id="Map">
                        <area shape="poly" coords="65,458,157,506,250,514,274,500,282,424,192,391,97,384">
                        <area shape="poly" coords="108,372,172,269,462,233,506,407,406,430,299,426,192,382,115,382">
                        <area shape="poly" coords="186,262,456,227,601,152,582,108,401,76,374,81,242,170,184,251">
                        <area shape="poly" coords="471,227,603,158,699,317,612,399,517,408,471,235">
                    </map>
                  </div>
                </article>
            </section>
    <!-- --------------------------------------siteCon------------------------------------ -->
            <section class="siteCon">    
                <div class="actTitle"><h2>忽浴推薦</h2></div>
                <div class="areaName"><h2>北投市場周邊</h2><img class="house" src="images/house.png" alt=""></div>
                <p class="remind"><span><i class="fas fa-bell"></i></span>點擊圖片看更多細節</p>
                <div class="siteDisplayCon">
                    <div class="siteDisplay" id="siteDisplay">
                        <div class="siteGroup" id="siteGroup">

                            <?php foreach($stmt as $row): ?>
                            <div class="site site-<?= $row['type'] ?>">
                                <div class="siteImg">
                                    <img src="images/viewpoint/<?= $row['photo'] ?>" alt="">
                                </div>
                                <div class="siteText">
                                    <div class="siteTitle">
                                        <h3 class="siteName"><?= $row['name'] ?></h3>
                                    </div>
                                    <hr>
                                    <div class="siteFoot">
                                        <ul class="siteInfo">
                                            <li><span><i class="fas fa-map-marker-alt"></i></span><a><?= $row['address'] ?></a></li>
                                            <li><span><i class="fas fa-phone-alt"></i></span><a class="phoneNum" href="<?= $row['tel_href'] ?>" ><?= $row['phone'] ?></a></li>   
                                            <li><span><i class="fas fa-clock"></i></span><p><?= $row['open'] ?></p></li>   
                                        </ul>
                                        <p class="introText" style="display:none"><?= $row['intro'] ?></p>
                                        <p class="mapText" style="display:none"><?= $row['map'] ?></p>
                                        <form class="siteAct <?=$row['sid']==$row['which'] && $row['belong']==$member_sid ? "liked" : "" ?>" onsubmit="return areaLike()">
                                            <input type="hidden" name="areaSid" value="<?= $row['sid'] ?>">
                                            <span><i class="fas fa-heart"></i></span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>

                        </div>
                        <div class="scrollBarBox" id="scrollBarBox"><div class="scrollBar" id="scrollBar"></div></div>
                    </div>
                </div>
                <div class="siteWindowCon" style="display:none">
                    <div class="siteWindow">
                        <div class="siteWindowClose"><i class="fas fa-times fa-fw"></i></div>
                        <div class="siteWindowContent">
                            <div class="siteWindowImg">
                                <img src="" alt="">
                            </div>
                            <div class="siteWindowTitle">
                                <h3 class="siteWindowName"></h3>
                                <hr>
                            </div>
                            <ul class="siteWindowInfo">
                                <li><span><i class="fas fa-map-marker-alt"></i></span><a></a></li>
                                <li><span><i class="fas fa-phone-alt"></i></span><a class="phoneNum" href="02-2896-0626"></a></li>   
                                <li><span><i class="fas fa-clock"></i></span><p></p></li><hr>
                                <li><p></p></li>
                            </ul>
                            <iframe src="" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>

<?php require __DIR__.'/__html_js.php'?>

    <script src="js/jquery.rwdImageMaps.min.js"></script>
    <script src="js/map.js"></script>
    <script>
        

        var areaSid =""
        function areaLike() {
            $.post("php_api/area_like_api.php",{"areaSid":areaSid}, function(data){ 
                if(data.success){
                    $(".successText").text(data.info);
                    $(".success").fadeIn().delay(500).fadeOut();
                }else{
                    $(".errorText").text(data.info);
                    $(".error").fadeIn().delay(800).fadeOut();
                }
            },"json")
            return false
        }

        $("html").on("click",".siteAct",function(){
            <?php if(isset($_SESSION['user'])){?>         
                areaSid = $(this).find("input").val()
                $(this).addClass("liked")
                $(this).submit()
            <?php }else{ ?>
                $(".errorText").text("收藏功能需登入才能使用");
                $(".error").fadeIn().delay(1000).fadeOut();
                $(".bgBlur").fadeIn();
            <?php }; ?>
        });



        $(".siteImg").click(function(){
            let siteImg = $(this).find("img").attr("src")
            let siteName = $(this).next(".siteText").find(".siteName").text()
            let siteInfo = $(this).next(".siteText").find(".siteInfo").html()
            let introText = $(this).next(".siteText").find(".introText").text()
            let mapText = $(this).next(".siteText").find(".mapText").text()

            let intro = $("<li></li>")
            intro.html("<p>"+introText+"</p>")

            $(".siteWindowImg img").attr("src",siteImg);
            $(".siteWindowName").text(siteName)
            $(".siteWindowInfo").html(siteInfo)
            $(".siteWindowInfo").append("<hr>")
            $(".siteWindowInfo").append(intro)
            $("iframe").attr("src",mapText)

            $(".siteWindowCon").fadeIn()
        })

        $(".siteWindowClose").click(function(){
            $(".siteWindowCon").fadeOut()
        })
    </script>
</body>
</html>